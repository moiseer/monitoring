﻿using System;

namespace Monitoring.Contracts
{
    /// <summary>
    /// Уровень критичности события.
    /// </summary>
    public enum DeviceEventLevel
    {
        /// <summary>
        /// Низкий уровень критичности.
        /// </summary>
        Low = 0,

        /// <summary>
        /// Средний уровень критичности.
        /// </summary>
        Medium = 1,

        /// <summary>
        /// Высокий уровень критичности.
        /// </summary>
        High = 2
    }
}
