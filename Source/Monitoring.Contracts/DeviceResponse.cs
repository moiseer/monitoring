﻿using System;

namespace Monitoring.Contracts
{
    /// <summary>
    /// Отправляемая статистическая информация об устройстве.
    /// </summary>
    public class DeviceResponse
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Дата и время статистики.
        /// </summary>
        public DateTime LastStatisticDate { get; set; }

        /// <summary>
        /// Имя узла.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Тип устройства, версия операционной системы.
        /// </summary>
        public string OperationSystem { get; set; }

        /// <summary>
        /// Версия приложения.
        /// </summary>
        public string Version { get; set; }
    }
}
