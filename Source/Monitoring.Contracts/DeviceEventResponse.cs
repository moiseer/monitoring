﻿using System;

namespace Monitoring.Contracts
{
    /// <summary>
    /// Отправляемое событие устройства.
    /// </summary>
    public class DeviceEventResponse
    {
        /// <summary>
        /// Дата и время возникновения.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Уровень критичности.
        /// </summary>
        public DeviceEventLevel Level { get; set; }

        /// <summary>
        /// Название.
        /// </summary>
        public string Name { get; set; }
    }
}
