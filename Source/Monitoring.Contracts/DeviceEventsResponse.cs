﻿using System.Collections.Generic;

namespace Monitoring.Contracts
{
    /// <summary>
    /// Отправляемый список событий устройства.
    /// </summary>
    public class DeviceEventsResponse
    {
        /// <summary>
        /// Список событий устройства.
        /// </summary>
        public List<DeviceEventResponse> Events { get; set; }
    }
}
