using System;
using MediatR;
using ProtoBuf;

namespace Monitoring.Contracts.ProtoBuf
{
    /// <summary>
    /// Базовый интерфейс для ProtoBuf контрактов.
    /// </summary>
    [ProtoContract]
    [ProtoInclude(1, typeof(NotifyDeviceEventProtoBuf))]
    public interface IProtoBufContract : IRequest
    {
    }
}
