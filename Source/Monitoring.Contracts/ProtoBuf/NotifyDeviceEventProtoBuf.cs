﻿using System;
using ProtoBuf;

namespace Monitoring.Contracts.ProtoBuf
{
    /// <summary>
    /// Событие устройства, сериализуемое в формате ProtoBuf.
    /// </summary>
    [ProtoContract]
    public class NotifyDeviceEventProtoBuf : IProtoBufContract
    {
        /// <summary>
        /// Дата и время возникновения.
        /// </summary>
        [ProtoMember(2)]
        public DateTime Date { get; set; }

        /// <summary>
        /// Идентификатор устройства.
        /// </summary>
        [ProtoMember(4)]
        public Guid DeviceId { get; set; }

        /// <summary>
        /// Уровень критичности.
        /// </summary>
        [ProtoMember(3)]
        public DeviceEventLevel Level { get; set; }

        /// <summary>
        /// Название.
        /// </summary>
        [ProtoMember(1)]
        public string Name { get; set; }
    }
}
