﻿using System;

namespace Monitoring.Contracts
{
    /// <summary>
    /// Запрос на обновление имени узла устройства.
    /// </summary>
    public class UpdateDeviceNameRequest
    {
        /// <summary>
        /// Идентификатор устройства.
        /// </summary>
        public Guid DeviceId { get; set; }

        /// <summary>
        /// Новое имя узла.
        /// </summary>
        public string Name { get; set; }
    }
}
