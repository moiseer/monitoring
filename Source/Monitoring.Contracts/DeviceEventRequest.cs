﻿using System;

namespace Monitoring.Contracts
{
    /// <summary>
    /// Принимаемое событие устройства.
    /// </summary>
    public class DeviceEventRequest
    {
        /// <summary>
        /// Дата и время возникновения.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Уровень критичности.
        /// </summary>
        public DeviceEventLevel Level { get; set; }

        /// <summary>
        /// Название.
        /// </summary>
        public string Name { get; set; }
    }
}
