﻿using System;
using System.Collections.Generic;

namespace Monitoring.Contracts
{
    /// <summary>
    /// Получаемая статистическая информация об устройстве.
    /// </summary>
    public class DeviceRequest
    {
        /// <summary>
        /// Список событий устройства.
        /// </summary>
        public List<DeviceEventRequest> Events { get; set; }

        /// <summary>
        /// Идентификатор.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Дата и время статистики.
        /// </summary>
        public string LastStatisticDate { get; set; }

        /// <summary>
        /// Имя узла.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Тип устройства, версия операционной системы.
        /// </summary>
        public string Os { get; set; }

        /// <summary>
        /// Версия приложения.
        /// </summary>
        public string Version { get; set; }
    }
}
