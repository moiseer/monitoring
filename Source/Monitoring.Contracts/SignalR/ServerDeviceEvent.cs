﻿using System;

namespace Monitoring.Contracts.SignalR
{
    /// <summary>
    /// Событие устройства на сервере.
    /// </summary>
    public class ServerDeviceEvent
    {
        /// <summary>
        /// Идентификатор устройства.
        /// </summary>
        public Guid DeviceId { get; set; }
        
        /// <summary>
        /// Тип события.
        /// </summary>
        public ServerDeviceEventType Type { get; set; }
    }
}
