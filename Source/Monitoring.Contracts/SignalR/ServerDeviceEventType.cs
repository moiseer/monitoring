﻿using System;

namespace Monitoring.Contracts.SignalR
{
    /// <summary>
    /// Тип события устройства на сервере.
    /// </summary>
    public enum ServerDeviceEventType
    {
        /// <summary>
        /// Информация об устройстве обновлена.
        /// </summary>
        DeviceUpdated,

        /// <summary>
        /// Информация об устройстве удалена.
        /// </summary>
        DeviceDeleted,

        /// <summary>
        /// События устройства обновлены.
        /// </summary>
        DeviceEventsUpdated
    }
}
