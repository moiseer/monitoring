﻿import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { DeviceService } from '../services/device.service';

export function deviceNameUniqueValidator(deviceService: DeviceService): AsyncValidatorFn {
  return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
    return deviceService.checkDeviceNameUnique(control.value.toString()).pipe(
      map(response => {
        return response ? null : {deviceNameUnique: { valid: false, value: control.value }};
      })
    );
  };
}
