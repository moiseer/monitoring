import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { filter, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { Device } from '../../models/device';
import { DeviceService } from '../../services/device.service';
import { deviceNameUniqueValidator } from '../../validators/device-name-unique.validator';
import { ServerDeviceEventType } from '../../models/server-device-event-type.enum';
import { SignalRService } from '../../services/signalr.service';

@Component({
  selector: 'app-device-name-editor',
  templateUrl: './device-name-editor.component.html',
  styleUrls: ['./device-name-editor.component.less']
})
export class DeviceNameEditorComponent implements OnInit, OnDestroy {

  device: Device;
  updateNameForm: FormGroup;
  destroy$: Subject<void> = new Subject<void>();

  constructor(
    private deviceService: DeviceService,
    private signalRService: SignalRService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<DeviceNameEditorComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    this.device = data.device;
  }

  ngOnInit() {
    this.formInit();
    this.subscribeToSignalR();
  }

  formInit() {
    this.updateNameForm = this.fb.group({
      id: [this.device.id, [Validators.required]],
      name: [
        this.device.name,
        [
          Validators.required,
          Validators.maxLength(50)
        ],
        deviceNameUniqueValidator(this.deviceService)
      ]
    });
  }

  updateName() {
    if (this.updateNameForm.invalid) {
      return;
    }

    this.deviceService.updateDeviceName(this.updateNameForm.value)
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => this.dialogRef.close(this.updateNameForm.value));
  }

  close() {
    this.dialogRef.close();
  }

  subscribeToSignalR() {
    this.signalRService.event$
      .pipe(
        filter(serverEvent => serverEvent.deviceId === this.device.id),
        takeUntil(this.destroy$)
      )
      .subscribe(
        serverEvent => {
          if (serverEvent.type === ServerDeviceEventType.DeviceUpdated) {
            console.log(`[DeviceNameEditor] Device ${serverEvent.deviceId} updated.`);
          } else if (serverEvent.type === ServerDeviceEventType.DeviceDeleted) {
            console.log(`[DeviceNameEditor] Device ${serverEvent.deviceId} deleted.`);
            this.close();
          }
        });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
