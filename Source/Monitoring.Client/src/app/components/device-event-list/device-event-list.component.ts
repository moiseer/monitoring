﻿import { Component, OnInit, OnChanges, Input, OnDestroy } from '@angular/core';
import { filter, map, takeUntil } from 'rxjs/operators';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';

import { DeviceService } from '../../services/device.service';
import { DeviceEvent } from '../../models/device-event';
import { ServerDeviceEventType } from '../../models/server-device-event-type.enum';
import { SignalRService } from '../../services/signalr.service';

@Component({
  selector: 'app-device-event-list',
  templateUrl: './device-event-list.component.html',
  styleUrls: ['./device-event-list.component.less']
})
export class DeviceEventListComponent implements  OnInit, OnChanges, OnDestroy {

  @Input() deviceId: string;
  deviceEvents: BehaviorSubject<DeviceEvent[]> = new BehaviorSubject<DeviceEvent[]>([]);
  filteredDeviceEvents: BehaviorSubject<DeviceEvent[]> = new BehaviorSubject<DeviceEvent[]>([]);
  destroy$: Subject<void> = new Subject<void>();

  displayedColumns: string[] = ['date', 'name'];
  eventLevelOptions = [
    {value: 0, name: 'Все события'},
    {value: 2, name: 'Только критичные'}
  ];
  selectedLevel: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  constructor(
    private deviceService: DeviceService,
    private signalRService: SignalRService
  ) {
  }

  get hasEvents(): boolean {
    return this.deviceEvents.value.length > 0;
  }

  ngOnInit(): void {
    this.subscribeToSignalR();
    this.subscribeFiltered();
  }

  ngOnChanges(): void {
    this.loadDeviceEvents();
  }

  loadDeviceEvents(): void {
    if (!this.deviceId) {
      return;
    }

    this.deviceService.getEventsForDevice(this.deviceId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: DeviceEvent[]) => {
        this.deviceEvents.next(data);
      });
  }

  deleteDeviceEvents(): void {
    this.deviceService.deleteEventsForDevice(this.deviceId)
      .pipe(takeUntil(this.destroy$))
      .subscribe();
  }

  subscribeToSignalR(): void {
    this.signalRService.event$
      .pipe(
        filter(serverEvent => serverEvent.deviceId === this.deviceId),
        takeUntil(this.destroy$)
      )
      .subscribe(
        serverEvent => {
          if (serverEvent.type === ServerDeviceEventType.DeviceEventsUpdated) {
            console.log(`[DeviceEventList] Device Events ${serverEvent.deviceId} updated.`);
            this.loadDeviceEvents();
          } else if (serverEvent.type === ServerDeviceEventType.DeviceDeleted) {
            console.log(`[DeviceEventList] Device ${serverEvent.deviceId} deleted.`);
            this.deviceId = null;
          }
        });
  }

  subscribeFiltered(): void {
    combineLatest(this.deviceEvents, this.selectedLevel)
      .pipe(
        map(([deviceEvents, level]) => deviceEvents.filter(deviceEvent => deviceEvent.level >= level)),
        takeUntil(this.destroy$)
      )
      .subscribe(deviceEvents => this.filteredDeviceEvents.next(deviceEvents));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
