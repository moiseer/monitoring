﻿import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { filter, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { DeviceService } from '../../services/device.service';
import { Device } from '../../models/device';
import { DeviceNameEditorComponent } from '../device-name-editor/device-name-editor.component';
import { SignalRService } from '../../services/signalr.service';
import { ServerDeviceEventType } from '../../models/server-device-event-type.enum';

@Component({
  selector: 'app-device-details',
  templateUrl: './device-details.component.html',
  styleUrls: ['./device-details.component.less']
})
export class DeviceDetailsComponent implements  OnInit, OnChanges, OnDestroy {

  @Input() deviceId: string;
  device: Device;
  dialogRef: MatDialogRef<DeviceNameEditorComponent>;
  destroy$: Subject<void> = new Subject<void>();

  constructor(
    private deviceService: DeviceService,
    private signalRService: SignalRService,
    private router: Router,
    private dialogModel: MatDialog
  ) { }

  ngOnInit() {
    this.subscribeToSignalR();
  }

  ngOnChanges() {
    this.loadDevice();
  }

  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {device: this.device};

    this.dialogRef = this.dialogModel.open(DeviceNameEditorComponent, dialogConfig);
  }

  loadDevice() {
    if (this.deviceId) {
      this.deviceService.getDevice(this.deviceId)
        .pipe(takeUntil(this.destroy$))
        .subscribe((device: Device) => this.device = device);
    }
  }

  subscribeToSignalR() {
    this.signalRService.event$
      .pipe(
        filter(serverEvent => serverEvent.deviceId === this.deviceId),
        takeUntil(this.destroy$)
      )
      .subscribe(
        serverEvent => {
          if (serverEvent.type === ServerDeviceEventType.DeviceUpdated) {
            console.log(`[DeviceDetails] Device ${serverEvent.deviceId} updated.`);
            this.loadDevice();
          } else if (serverEvent.type === ServerDeviceEventType.DeviceDeleted) {
            console.log(`[DeviceDetails] Device ${serverEvent.deviceId} deleted.`);
            this.deviceId = null;
            this.router.navigate(['/']);
          }
        });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
