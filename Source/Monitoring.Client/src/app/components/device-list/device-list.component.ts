﻿import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { DeviceService } from '../../services/device.service';
import { Device } from '../../models/device';
import { ServerDeviceEventType } from '../../models/server-device-event-type.enum';
import { SignalRService } from '../../services/signalr.service';

@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.less']
})
export class DeviceListComponent implements  OnInit, OnDestroy {

  @Input() selectedDeviceId: string;
  devices: Device[];
  destroy$: Subject<void> = new Subject<void>();

  displayedColumns: string[] = ['name', 'lastStatisticDate', 'version', 'operationSystem', 'details'];

  constructor(
    private deviceService: DeviceService,
    private signalRService: SignalRService
  ) { }

  ngOnInit(): void {
    this.loadDevices();
    this.subscribeToSignalR();
  }

  loadDevices() {
    this.deviceService.getDevices()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: Device[]) => this.devices = data);
  }

  subscribeToSignalR() {
    this.signalRService.event$
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        serverEvent => {
          if (serverEvent.type === ServerDeviceEventType.DeviceUpdated) {
            console.log(`[DeviceList] Device ${serverEvent.deviceId} updated.`);
            this.loadDevices();
          } else if (serverEvent.type === ServerDeviceEventType.DeviceDeleted) {
            console.log(`[DeviceList] Device ${serverEvent.deviceId} deleted.`);
            this.loadDevices();
          }
        });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
