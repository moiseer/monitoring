import { Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder, LogLevel } from '@microsoft/signalr';
import { Subject } from 'rxjs';

import { ServerDeviceEvent } from '../models/server-device-event';

@Injectable({
  providedIn: 'root'
})
export class SignalRService {

  public event$: Subject<ServerDeviceEvent> = new Subject<ServerDeviceEvent>();

  private url = '/clientHub';
  private connection: HubConnection;

  constructor() {
    this.startConnection();
    this.connectionOn();
  }

  startConnection() {
    this.connection = new HubConnectionBuilder()
      .configureLogging(LogLevel.Debug)
      .withUrl(this.url)
      .build();
    this.connection.start()
      .then(() => console.log('Connected to ', this.url))
      .catch(err => console.log(`Error while starting connection. ${err}`));
  }

  stopConnection() {
    this.connection.stop()
      .then(() => console.log('Connection is stopping.'))
      .catch(err => console.log(`Error while stopping connection. ${err}`));
  }

  connectionOn() {
    this.connection.on('ServerEvent', (event) => {
      console.log(event);
      this.event$.next(event);
    });
  }
}
