import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Device } from '../models/device';
import { DeviceEvent } from '../models/device-event';
import { UpdateDeviceNameResponse } from '../models/update-device-name-response';
import { DeviceEventsResponse } from '../models/device-events-response';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  private url = '/api/device/';

  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) {
  }

  deleteEventsForDevice(deviceId: string): Observable<any>  {
    return this.http.delete(this.url + deviceId + '/events');
  }

  getDevice(deviceId: string): Observable<Device> {
    return this.http.get<Device>(this.url + deviceId);
  }

  getDevices(): Observable<Device[]> {
    return this.http.get<Device[]>(this.url);
  }

  getEventsForDevice(deviceId): Observable<DeviceEvent[]> {
    return this.http.get<DeviceEventsResponse>(this.url + deviceId + '/events')
      .pipe(map(response => response.events));
  }

  updateDeviceName(device: Device): Observable<any> {
    const response: UpdateDeviceNameResponse = {
      deviceId: device.id,
      name: device.name
    };
    return this.http.patch(this.url, response, this.httpOptions);
  }

  checkDeviceNameUnique(name: string): Observable<any> {
    return this.http.get(this.url + name + '/unique');
  }
}
