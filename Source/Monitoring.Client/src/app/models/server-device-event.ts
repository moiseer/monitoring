import { ServerDeviceEventType } from './server-device-event-type.enum';

export interface ServerDeviceEvent {
    deviceId: string;
    type: ServerDeviceEventType;
}
