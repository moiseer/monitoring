export enum DeviceEventLevel {
  Low = 0,
  Medium = 1,
  High = 2
}
