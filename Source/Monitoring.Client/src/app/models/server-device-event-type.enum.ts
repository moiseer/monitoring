export enum ServerDeviceEventType {
    DeviceUpdated,
    DeviceDeleted,
    DeviceEventsUpdated
}
