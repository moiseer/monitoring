﻿import { DeviceEvent } from './device-event';

export interface DeviceEventsResponse {
  events: DeviceEvent[];
}
