﻿export interface UpdateDeviceNameResponse {
  deviceId: string;
  name: string;
}
