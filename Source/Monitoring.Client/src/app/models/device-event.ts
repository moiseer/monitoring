﻿import { DeviceEventLevel } from './device-event-level.enum';

export interface DeviceEvent {
  name: string;
  date: Date;
  level: DeviceEventLevel;
}
