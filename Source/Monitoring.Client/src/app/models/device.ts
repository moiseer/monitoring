﻿export interface Device {
  id: string;
  name: string;
  lastStatisticDate: Date;
  operationSystem: string;
  version: string;
}
