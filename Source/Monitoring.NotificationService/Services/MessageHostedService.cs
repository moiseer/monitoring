﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Monitoring.NotificationService.MessageHandler;
using Serilog;

namespace Monitoring.NotificationService.Services
{
    /// <summary>
    /// Интерфейс фонового сервиса для работы с сообщениями.
    /// </summary>
    public class MessageHostedService : IHostedService
    {
        private readonly ILogger logger = Log.ForContext<MessageHostedService>();
        private readonly IMessageHandler messageHandler;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="messageHandler">Обработчик сообщений в систему обмена сообщениями.</param>
        public MessageHostedService(IMessageHandler messageHandler)
        {
            this.messageHandler = messageHandler;
        }

        /// <summary>
        /// Запуск работы сервиса.
        /// </summary>
        /// <param name="cancellationToken">Токен отмены задачи.</param>
        /// <returns>Завершенная задача.</returns>
        public Task StartAsync(CancellationToken cancellationToken)
        {
            logger.Information("MessageHostedService is starting.");

            messageHandler.StartListen();

            return Task.CompletedTask;
        }

        /// <summary>
        /// Остановка работы сервиса.
        /// </summary>
        /// <param name="cancellationToken">Токен отмены задачи.</param>
        /// <returns>Завершенная задача.</returns>
        public Task StopAsync(CancellationToken cancellationToken)
        {
            logger.Information("MessageHostedService is stopping.");

            messageHandler.StopListen();
            return Task.CompletedTask;
        }
    }
}
