using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Monitoring.NotificationService
{
    /// <summary>
    /// Класс, содержащий точку входа.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Создание строителя веб-узла.
        /// </summary>
        /// <param name="args">Аргументы командной строки.</param>
        /// <returns>Строитель для веб-узла.</returns>
        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
        }

        /// <summary>
        /// Точка входа для выполняемой программы.
        /// </summary>
        /// <param name="args">Аргументы командной строки.</param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }
    }
}
