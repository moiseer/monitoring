﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Monitoring.Contracts.ProtoBuf;

namespace Monitoring.NotificationService.Mediator.Handlers
{
    /// <summary>
    /// Обработчик оповещения о событии устройства.
    /// </summary>
    public class NotifyDeviceEventHandler : IRequestHandler<NotifyDeviceEventProtoBuf>
    {
        /// <summary>
        /// Обработка запроса.
        /// </summary>
        /// <param name="request">Событие устройства.</param>
        /// <param name="cancellationToken">Токен отмены задачи.</param>
        /// <returns>Задача, сообщающая об окончании обработки.</returns>
        public Task<Unit> Handle(NotifyDeviceEventProtoBuf request, CancellationToken cancellationToken)
        {
            Console.WriteLine($"Получено сообщение:\r\n" +
                $"{request.Date}|{request.Level}|{request.DeviceId}|{request.Name}");

            return Task.FromResult(Unit.Value);
        }
    }
}
