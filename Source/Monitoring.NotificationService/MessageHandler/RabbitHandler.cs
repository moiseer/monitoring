﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Configuration;
using Monitoring.Contracts.ProtoBuf;
using ProtoBuf;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.MessagePatterns;
using Serilog;

namespace Monitoring.NotificationService.MessageHandler
{
    /// <summary>
    /// Обработчик сообщений из службы брокера сообщений RabbitMQ.
    /// </summary>
    public class RabbitHandler : IMessageHandler
    {
        private readonly CancellationTokenSource cancellationTokenSource;
        private readonly string connectionString;
        private readonly string exchangeName;
        private readonly ILogger logger;
        private readonly string queueName;
        private readonly string routingKey;
        private readonly IMediator mediator;
        private IModel channel;
        private IConnection connection;
        private Task task;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="configuration">Конфигурация подключеиня к службе брокера сообщений.</param>
        /// <param name="mediator">Объект-посредник, выполняющий передачу запросов в соответствующий обоаботчик.</param>
        public RabbitHandler(IConfiguration configuration, IMediator mediator)
        {
            connectionString = configuration["RabbitConnectionString"];
            exchangeName = configuration["ExchangeName"];
            queueName = configuration["QueueName"];
            routingKey = configuration["RoutingKey"];

            cancellationTokenSource = new CancellationTokenSource();
            logger = Log.ForContext<RabbitHandler>();
            this.mediator = mediator;
        }

        /// <summary>
        /// Освобождение неуправляемых ресурсов.
        /// </summary>
        public void Dispose()
        {
            logger.Information("RabbitHandler is disposing.");

            cancellationTokenSource?.Dispose();
            channel?.Dispose();
            connection?.Dispose();
        }

        /// <summary>
        /// Начать получать сообщения из системы обмена сообщениями.
        /// </summary>
        public void StartListen()
        {
            logger.Information("RabbitHandler is starting listen.");

            connection = GetRabbitConnection(connectionString);
            channel = GetRabbitChannel(connection, exchangeName, queueName, routingKey);

            task = new Task(() => Listen(cancellationTokenSource.Token));
            task.Start();
        }

        /// <summary>
        /// Закончить получать сообщения из системы обмена сообщениями.
        /// </summary>
        public void StopListen()
        {
            logger.Information("RabbitHandler is stopping listen.");

            cancellationTokenSource?.Cancel();
            task?.Wait(1000);

            logger.Information("RabbitHandler is closing connection.");
            channel?.Close();
            connection?.Close();
        }

        private IModel GetRabbitChannel(IConnection connection, string exchangeName, string queueName, string routingKey)
        {
            IModel model = connection.CreateModel();
            model.ExchangeDeclare(exchangeName, ExchangeType.Direct);
            model.QueueDeclare(queueName, false, false, false, null);
            model.QueueBind(queueName, exchangeName, routingKey, null);
            return model;
        }

        private IConnection GetRabbitConnection(string connectionString)
        {
            ConnectionFactory factory = new ConnectionFactory
            {
                Uri = new Uri(connectionString)
            };

            return factory.CreateConnection();
        }

        private void Listen(CancellationToken cancellationToken)
        {
            using (var subscription = new Subscription(channel, queueName, false))
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    BasicDeliverEventArgs basicDeliveryEventArgs = subscription.Next();

                    if (cancellationToken.IsCancellationRequested)
                    {
                        break;
                    }

                    try
                    {
                        IProtoBufContract request =
                            Serializer.Deserialize<IProtoBufContract>(basicDeliveryEventArgs.Body);

                        mediator.Send(request);
                    }
                    catch (Exception e)
                    {
                        logger.Error("Unexpected error: {Error}", e);
                        Console.WriteLine(e.Message);
                    }

                    subscription.Ack(basicDeliveryEventArgs);
                }
            }
        }
    }
}
