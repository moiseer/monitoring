﻿using System;

namespace Monitoring.NotificationService.MessageHandler
{
    /// <summary>
    /// Интерфейс обработчика сообщений из системы обмена сообщениями.
    /// </summary>
    public interface IMessageHandler : IDisposable
    {
        /// <summary>
        /// Начать получать сообщения из системы обмена сообщениями.
        /// </summary>
        void StartListen();

        /// <summary>
        /// Закончить получать сообщения из системы обмена сообщениями.
        /// </summary>
        void StopListen();
    }
}
