﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace Monitoring.NotificationService.Controllers
{

    /// <summary>
    /// Контроллер для работы с сообщениями и оповещениями.
    /// </summary>
    [ApiController]
    [Route("[message]")]
    public class MessageController : ControllerBase
    {
    }
}
