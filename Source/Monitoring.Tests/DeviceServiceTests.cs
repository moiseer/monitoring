﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Monitoring.Contracts;
using Monitoring.Contracts.SignalR;
using Monitoring.Mvc.Core;
using Monitoring.Mvc.Models;
using Monitoring.Mvc.Repositories;
using Monitoring.Mvc.Services;
using Monitoring.Mvc.Uow;
using Monitoring.Mvc.Validators;
using Moq;
using Xunit;

namespace Monitoring.Tests
{
    /// <summary>
    /// Тесты для класса DeviceService.
    /// </summary>
    public class DeviceServiceTests
    {
        private readonly Mock<IClientNotifyService> clientNotifyServiceMock;
        private readonly Mock<IDeviceEventRepository> deviceEvensRepositoryMock;
        private readonly Mock<IDeviceRepository> deviceRepositoryMock;
        private readonly Guid existingId;
        private readonly string existName = "name1";
        private readonly IDeviceService service;
        private readonly Mock<IUnitOfWork> unitOfWorkMock;
        private readonly string userName = "UserName";
        private readonly Guid userNameDeviceId;

        /// <summary>
        /// Конструктор.
        /// </summary>
        public DeviceServiceTests()
        {
            existingId = Guid.NewGuid();
            userNameDeviceId = Guid.NewGuid();

            deviceRepositoryMock = new Mock<IDeviceRepository>();
            deviceEvensRepositoryMock = new Mock<IDeviceEventRepository>();
            unitOfWorkMock = new Mock<IUnitOfWork>();
            var unitOfWorkFactoryMock = new Mock<IUnitOfWorkFactory>();
            var validatorMock = new Mock<IDeviceValidator>();

            deviceRepositoryMock.Setup(x => x.Get(existingId))
                .ReturnsAsync((Guid id) => new Device { Id = id });
            deviceRepositoryMock.Setup(x => x.Get(userNameDeviceId))
                .ReturnsAsync((Guid id) =>
                    new Device { Id = id, Name = userName, IsUserDefinedName = true });
            unitOfWorkMock.Setup(x => x.GetDeviceRepository())
                .Returns(deviceRepositoryMock.Object);
            unitOfWorkMock.Setup(x => x.GetDeviceEventRepository())
                .Returns(deviceEvensRepositoryMock.Object);
            unitOfWorkFactoryMock.Setup(x => x.Create())
                .Returns(unitOfWorkMock.Object);
            validatorMock.Setup(x => x.ValidateNameUnique(existName, It.IsAny<IDeviceRepository>()))
                .Throws(new MonitoringException());

            var mediatorMock = new Mock<IMediator>();
            var messageServiceMock = new Mock<IMessageService>();
            clientNotifyServiceMock = new Mock<IClientNotifyService>();

            service = new DeviceService(
                unitOfWorkFactoryMock.Object,
                validatorMock.Object,
                mediatorMock.Object,
                messageServiceMock.Object,
                clientNotifyServiceMock.Object);
        }

        /// <summary>
        /// Тест на вызов метода обновления устройства если Id не существует в базе.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        [Fact]
        public async Task AddOrUpdate_AddNewDeviceToRepository_DeviceAdded()
        {
            DeviceRequest device = GetDeviceRequest(Guid.NewGuid());

            await service.AddOrUpdate(device);

            deviceRepositoryMock.Verify(x => x.Add(It.IsAny<Device>()), Times.Once);
            deviceRepositoryMock.Verify(x => x.Update(It.IsAny<Device>()), Times.Never);
            unitOfWorkMock.Verify(x => x.Commit(), Times.Once);
            deviceEvensRepositoryMock.Verify(x => x.Add(It.IsAny<DeviceEvent>()), Times.Exactly(2));

            clientNotifyServiceMock.Verify(x => x.Notify(device.Id, ServerDeviceEventType.DeviceUpdated), Times.Once);
            clientNotifyServiceMock.Verify(x => x.Notify(device.Id, ServerDeviceEventType.DeviceEventsUpdated), Times.Once);
        }

        /// <summary>
        /// Тест, что имя узла, назначенное системой, обновилось.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        [Fact]
        public async Task AddOrUpdate_IsUserDefinedNameFalse_NameUpdated()
        {
            DeviceRequest device = GetDeviceRequest(existingId);
            var newName = "newName";
            device.Name = newName;

            await service.AddOrUpdate(device);

            deviceRepositoryMock.Verify(x => x.Update(It.Is<Device>(d => d.Name == newName)), Times.Once);
            unitOfWorkMock.Verify(x => x.Commit(), Times.Once);

            clientNotifyServiceMock.Verify(x => x.Notify(device.Id, ServerDeviceEventType.DeviceUpdated), Times.Once);
            clientNotifyServiceMock.Verify(x => x.Notify(device.Id, ServerDeviceEventType.DeviceEventsUpdated), Times.Once);
        }

        /// <summary>
        /// Тест, что имя узла, назначенное пользователем, не обновилось.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        [Fact]
        public async Task AddOrUpdate_IsUserDefinedNameTrue_NameNotUpdated()
        {
            DeviceRequest device = GetDeviceRequest(userNameDeviceId);
            var newName = "newName";
            device.Name = newName;

            await service.AddOrUpdate(device);

            deviceRepositoryMock.Verify(x => x.Update(It.Is<Device>(d => d.Name == userName)), Times.Once);
            unitOfWorkMock.Verify(x => x.Commit(), Times.Once);

            clientNotifyServiceMock.Verify(x => x.Notify(device.Id, ServerDeviceEventType.DeviceUpdated), Times.Once);
            clientNotifyServiceMock.Verify(x => x.Notify(device.Id, ServerDeviceEventType.DeviceEventsUpdated), Times.Once);
        }

        /// <summary>
        /// Тест на вызов метода обновления устройства если Id существует в базе.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        [Fact]
        public async Task AddOrUpdate_UpdateDeviceFromRepository_DeviceUpdated()
        {
            DeviceRequest device = GetDeviceRequest(existingId);

            await service.AddOrUpdate(device);

            deviceRepositoryMock.Verify(x => x.Update(It.IsAny<Device>()), Times.Once);
            deviceRepositoryMock.Verify(x => x.Add(It.IsAny<Device>()), Times.Never);
            deviceEvensRepositoryMock.Verify(x => x.Add(It.IsAny<DeviceEvent>()), Times.Exactly(2));
            unitOfWorkMock.Verify(x => x.Commit(), Times.Once);

            clientNotifyServiceMock.Verify(x => x.Notify(device.Id, ServerDeviceEventType.DeviceUpdated), Times.Once);
            clientNotifyServiceMock.Verify(x => x.Notify(device.Id, ServerDeviceEventType.DeviceEventsUpdated), Times.Once);
        }

        /// <summary>
        /// Тест на вызов метода удаления устройства из базе.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        [Fact]
        public async Task Delete_RemoveDeviceFromRepository_DeviceRemoved()
        {
            DeviceRequest device = GetDeviceRequest(existingId);

            await service.Delete(device.Id);

            deviceRepositoryMock.Verify(x => x.Delete(existingId), Times.Once);
            deviceEvensRepositoryMock.Verify(x => x.DeleteForDevice(existingId), Times.Once);
            unitOfWorkMock.Verify(x => x.Commit(), Times.Once);

            clientNotifyServiceMock.Verify(x => x.Notify(device.Id, ServerDeviceEventType.DeviceDeleted), Times.Once);
        }

        private DeviceRequest GetDeviceRequest(Guid id)
        {
            var device = new DeviceRequest
            {
                Id = id,
                Name = $"Test Node {id}",
                LastStatisticDate = DateTime.Today.ToString(),
                Version = "1.1.0.0",
                Os = "Win 10x",
                Events = new List<DeviceEventRequest>
                {
                    new DeviceEventRequest { Name = "App_Start", Date = DateTime.Today },
                    new DeviceEventRequest { Name = "StartVonComplete", Date = DateTime.Today }
                }
            };

            return device;
        }
    }
}
