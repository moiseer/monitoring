﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Monitoring.Mvc.Migrations;
using Monitoring.Mvc.Models;
using Monitoring.Mvc.Repositories;
using Monitoring.Mvc.Uow;
using Xunit;

namespace Monitoring.Tests
{
    /// <summary>
    /// Тесты для класса NHibernateDeviceRepository.
    /// </summary>
    public class NHibernateDeviceRepositoryTests
    {
        private readonly IUnitOfWorkFactory unitOfWorkFactory;

        /// <summary>
        /// Конструктор.
        /// </summary>
        public NHibernateDeviceRepositoryTests()
        {
            var dic = new Dictionary<string, string>
            {
                ["UseNHibernate"] = "true"
            };

            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("testsettings.json")
                .AddInMemoryCollection(dic)
                .Build();

            var connectionString = config["ConnectionString"];
            DbMigrator.StartMigration(connectionString);

            unitOfWorkFactory = new UnitOfWorkFactory(config);
        }

        /// <summary>
        /// Тест добавления данных об устройстве в БД.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        [Fact]
        public async Task Add_AddDeviceToRepository_DeviceAdded()
        {
            Device device = GetSomeDevice();

            using (var unitOfWork = unitOfWorkFactory.Create())
            {
                var repository = unitOfWork.GetDeviceRepository();
                await repository.Add(device);

                Device result = await repository.Get(device.Id);

                Assert.IsType<NHibernateDeviceRepository>(repository);
                Assert.Equal(device.Name, result.Name);
                Assert.Equal(device.LastStatisticDate.ToString(), result.LastStatisticDate.ToString());
                Assert.Equal(device.DateReceived.ToString(), result.DateReceived.ToString());
                Assert.Equal(device.Version, result.Version);
                Assert.Equal(device.OperationSystem, result.OperationSystem);
            }
        }

        /// <summary>
        /// Тест удаления данных об устройстве из БД.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        [Fact]
        public async Task Delete_RemoveDeviceFromRepository_DeviceRemoved()
        {
            Device device = GetSomeDevice();

            using (var unitOfWork = unitOfWorkFactory.Create())
            {
                var repository = unitOfWork.GetDeviceRepository();
                await repository.Add(device);

                await repository.Delete(device.Id);

                Device result = await repository.Get(device.Id);

                Assert.IsType<NHibernateDeviceRepository>(repository);
                Assert.Null(result);
            }
        }

        /// <summary>
        /// Тест получения списка данных о всех устройствах из БД.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        [Fact]
        public async Task Get_DeviceListRequest_DeviceListReturned()
        {
            Device insertDevice = GetSomeDevice();

            using (var unitOfWork = unitOfWorkFactory.Create())
            {
                var repository = unitOfWork.GetDeviceRepository();
                await repository.Add(insertDevice);
                List<Device> result = await repository.Get();

                Assert.IsType<NHibernateDeviceRepository>(repository);
                Assert.NotEmpty(result);
            }
        }

        /// <summary>
        /// Тест изменения данных об устройстве в БД.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        [Fact]
        public async Task Update_ChangeDeviceInRepository_DeviceUpdated()
        {
            Device insertDevice = GetSomeDevice();

            using (var unitOfWork = unitOfWorkFactory.Create())
            {
                var repository = unitOfWork.GetDeviceRepository();
                await repository.Add(insertDevice);
            }

            var device = new Device
            {
                Id = insertDevice.Id,
                Name = $"Test Node {insertDevice.Id} U",
                LastStatisticDate = DateTime.Today,
                DateReceived = DateTime.Now,
                Version = "1.1.0.0",
                OperationSystem = "Win 10x"
            };

            using (var unitOfWork = unitOfWorkFactory.Create())
            {
                var repository = unitOfWork.GetDeviceRepository();

                await repository.Update(device);

                Device result = await repository.Get(device.Id);

                Assert.IsType<NHibernateDeviceRepository>(repository);
                Assert.Equal(device.Name, result.Name);
                Assert.Equal(device.LastStatisticDate.ToString(), result.LastStatisticDate.ToString());
                Assert.Equal(device.DateReceived.ToString(), result.DateReceived.ToString());
                Assert.Equal(device.Version, result.Version);
                Assert.Equal(device.OperationSystem, result.OperationSystem);
            }
        }

        private Device GetSomeDevice()
        {
            Guid id = Guid.NewGuid();
            var device = new Device
            {
                Id = id,
                Name = $"Test Node {id}",
                LastStatisticDate = DateTime.Today,
                DateReceived = DateTime.Now,
                Version = "1.1.0.0",
                OperationSystem = "Win 10x"
            };

            return device;
        }
    }
}
