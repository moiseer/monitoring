﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Monitoring.Contracts;
using Monitoring.Mvc;
using Monitoring.Mvc.Uow;
using Newtonsoft.Json;
using Xunit;

namespace Monitoring.Tests
{
    /// <summary>
    /// Интеграционные тесты проекта <see cref="Monitoring.Mvc"/>.
    /// </summary>
    public class IntegrationTests
    {
        private readonly HttpClient client;
        private readonly IConfiguration config;

        /// <summary>
        /// Initializes a new instance of the <see cref="IntegrationTests"/> class.
        /// </summary>
        public IntegrationTests()
        {
            config = new ConfigurationBuilder()
                .AddJsonFile("testsettings.json")
                .Build();

            var server = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>()
                .UseConfiguration(config));

            client = server.CreateClient();

            ClearTestDb().Wait();
        }

        /// <summary>
        /// Тест на удаление устройства.
        /// </summary>
        /// <returns>
        /// <placeholder>A <see cref="Task"/> representing the asynchronous unit test.</placeholder>
        /// </returns>
        [Fact]
        public async Task Delete_DeleteDevice_DeviceDeleted()
        {
            var deviceId = Guid.NewGuid();
            DeviceRequest deviceRequest = CreateDeviceRequest(deviceId);

            var responsePut = await client.PutAsync("api/statistic",
                new StringContent(
                    JsonConvert.SerializeObject(deviceRequest),
                    Encoding.UTF8,
                    "application/json")
            );

            var responseDelete = await client.DeleteAsync($"api/statistic/{deviceId}");

            responsePut.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, responsePut.StatusCode);
            responseDelete.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, responseDelete.StatusCode);
        }

        /// <summary>
        /// Тест на обновление имени устройства.
        /// </summary>
        /// <returns>
        /// <placeholder>A <see cref="Task"/> representing the asynchronous unit test.</placeholder>
        /// </returns>
        [Fact]
        public async Task Patch_UpdateDeviceName_DeviceNameUpdated()
        {
            var deviceId = Guid.NewGuid();
            DeviceRequest deviceRequest = CreateDeviceRequest(deviceId);

            var responsePut = await client.PutAsync("api/statistic",
                new StringContent(
                    JsonConvert.SerializeObject(deviceRequest),
                    Encoding.UTF8,
                    "application/json")
            );

            UpdateDeviceNameRequest updateDeviceNameRequest = CreateUpdateDeviceNameRequest(deviceId);

            var responsePatch = await client.PatchAsync("api/device",
                new StringContent(
                    JsonConvert.SerializeObject(updateDeviceNameRequest),
                    Encoding.UTF8,
                    "application/json"));

            var responseGet = await client.GetAsync($"api/device/{deviceId}");
            var responseString = await responseGet.Content.ReadAsStringAsync();
            var deviceResponse = JsonConvert.DeserializeObject<DeviceResponse>(responseString);

            responsePut.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, responsePut.StatusCode);
            responsePatch.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, responsePatch.StatusCode);
            responseGet.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, responseGet.StatusCode);
            Assert.Equal(updateDeviceNameRequest.Name, deviceResponse.Name);
        }

        /// <summary>
        /// Тест на добавление устройства.
        /// </summary>
        /// <returns>
        /// <placeholder>A <see cref="Task"/> representing the asynchronous unit test.</placeholder>
        /// </returns>
        [Fact]
        public async Task Put_AddNewDevice_DeviceAdded()
        {
            var deviceId = Guid.NewGuid();
            DeviceRequest deviceRequest = CreateDeviceRequest(deviceId);

            var responsePut = await client.PutAsync("api/statistic",
                new StringContent(
                    JsonConvert.SerializeObject(deviceRequest),
                    Encoding.UTF8,
                    "application/json")
            );

            var responseGet = await client.GetAsync($"api/device/{deviceId}");
            var responseString = await responseGet.Content.ReadAsStringAsync();
            var deviceResponse = JsonConvert.DeserializeObject<DeviceResponse>(responseString);

            responsePut.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, responsePut.StatusCode);
            responseGet.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, responseGet.StatusCode);
            Assert.Equal(deviceRequest.Id, deviceResponse.Id);
            Assert.Equal(deviceRequest.Name, deviceResponse.Name);
            Assert.Equal(deviceRequest.Os, deviceResponse.OperationSystem);
            Assert.Equal(deviceRequest.Version, deviceResponse.Version);
            Assert.Equal(deviceRequest.LastStatisticDate, deviceResponse.LastStatisticDate.ToString());
        }

        /// <summary>
        /// Тест на удаление событий устройств.
        /// </summary>
        /// <returns>
        /// <placeholder>A <see cref="Task"/> representing the asynchronous unit test.</placeholder>
        /// </returns>
        [Fact]
        public async Task DeleteEventsForDevice_DeleteEvents_EventsDeleted()
        {
            var deviceId = Guid.NewGuid();
            DeviceRequest deviceRequest = CreateDeviceRequest(deviceId);
            deviceRequest.Events.Add(CreateDeviceEventRequest());
            
            var responsePut = await client.PutAsync("api/statistic",
                new StringContent(
                    JsonConvert.SerializeObject(deviceRequest),
                    Encoding.UTF8,
                    "application/json")
            );

            var responseDelete = await client.DeleteAsync($"api/device/{deviceId}/events");
            
            var responseGet = await client.GetAsync($"api/device/{deviceId}/events");
            var responseString = await responseGet.Content.ReadAsStringAsync();
            var deviceEventsResponse = JsonConvert.DeserializeObject<DeviceEventsResponse>(responseString);
            
            responsePut.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, responsePut.StatusCode);
            responseDelete.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, responseDelete.StatusCode);
            responseGet.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, responseGet.StatusCode);
            Assert.Empty(deviceEventsResponse.Events);
        }

        private static DeviceRequest CreateDeviceRequest(Guid deviceId)
        {
            return new DeviceRequest
            {
                Id = deviceId,
                LastStatisticDate = DateTime.Now.ToString(),
                Name = $"Test device {deviceId}",
                Os = "Device os",
                Version = "Device version",
                Events = new List<DeviceEventRequest>()
            };
        }

        private static UpdateDeviceNameRequest CreateUpdateDeviceNameRequest(Guid deviceId)
        {
            return new UpdateDeviceNameRequest
            {
                DeviceId = deviceId,
                Name = $"New test name {deviceId}"
            };
        }
        
        private static DeviceEventRequest CreateDeviceEventRequest()
        {
            return new DeviceEventRequest
            {
                Date = DateTime.Now,
                Level = 0,
                Name = "Event"
            };
        }

        private async Task ClearTestDb()
        {
            var unitOfWorkFactory = new UnitOfWorkFactory(config);

            using (var unitOfWork = unitOfWorkFactory.Create())
            {
                var deviceEventRepository = unitOfWork.GetDeviceEventRepository();
                var deviceRepository = unitOfWork.GetDeviceRepository();

                await deviceEventRepository.DeleteAll();
                await deviceRepository.DeleteAll();

                unitOfWork.Commit();
            }
        }
    }
}
