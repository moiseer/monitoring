﻿using System;
using System.Collections.Generic;
using Monitoring.Contracts;
using Monitoring.Contracts.ProtoBuf;
using Monitoring.Mvc.MessageSender;
using Monitoring.Mvc.Services;
using Moq;
using Xunit;

namespace Monitoring.Tests
{
    /// <summary>
    /// Тесты для класса MessageService.
    /// </summary>
    public class MessageServiceTests
    {
        /// <summary>
        /// Тест метода оповещения при отправке событий без высокого уровня критичности.
        /// </summary>
        [Fact]
        public void NotifyAboutHighLevelEvents_NoHighLevel_NoMessagesSent()
        {
            var mockMessageSender = new Mock<IMessageSender>();
            var messageService = new MessageService(mockMessageSender.Object);
            var events = new List<NotifyDeviceEventProtoBuf>
            {
                new NotifyDeviceEventProtoBuf { Level = DeviceEventLevel.Low },
                new NotifyDeviceEventProtoBuf { Level = DeviceEventLevel.Medium }
            };

            messageService.NotifyAboutHighLevelEvents(Guid.Empty, events);

            mockMessageSender.Verify(ms => ms.Send(It.IsAny<IProtoBufContract>()), Times.Never);
        }

        /// <summary>
        /// Тест метода оповещения при отправке событий с высоким уровнем критичности.
        /// </summary>
        [Fact]
        public void NotifyAboutHighLevelEvents_TwoHighLevel_TwoMessagesSent()
        {
            var mockMessageSender = new Mock<IMessageSender>();
            var messageService = new MessageService(mockMessageSender.Object);
            var events = new List<NotifyDeviceEventProtoBuf>
            {
                new NotifyDeviceEventProtoBuf { Level = DeviceEventLevel.High },
                new NotifyDeviceEventProtoBuf { Level = DeviceEventLevel.Low },
                new NotifyDeviceEventProtoBuf { Level = DeviceEventLevel.Medium },
                new NotifyDeviceEventProtoBuf { Level = DeviceEventLevel.High }
            };

            messageService.NotifyAboutHighLevelEvents(Guid.Empty, events);

            mockMessageSender.Verify(ms => ms.Send(It.IsAny<IProtoBufContract>()), Times.Exactly(2));
        }
    }
}
