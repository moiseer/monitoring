﻿using System;
using System.Threading.Tasks;
using Monitoring.Mvc.Core;
using Monitoring.Mvc.Repositories;
using Monitoring.Mvc.Validators;
using Moq;
using Xunit;

namespace Monitoring.Tests
{
    /// <summary>
    /// Тесты для класса DeviceValidator.
    /// </summary>
    public class DeviceValidatorTests
    {
        private readonly Mock<IDeviceRepository> deviceRepositoryMock;
        private readonly string existName = "name1";
        private readonly string newName = "name2";

        private readonly IDeviceValidator validator;

        /// <summary>
        /// Конструктор.
        /// </summary>
        public DeviceValidatorTests()
        {
            deviceRepositoryMock = new Mock<IDeviceRepository>();
            deviceRepositoryMock.Setup(x => x.IsNameUnique(existName))
                .ReturnsAsync(false);
            deviceRepositoryMock.Setup(x => x.IsNameUnique(newName))
                .ReturnsAsync(true);

            validator = new DeviceValidator();
        }

        /// <summary>
        /// Тест проверки уникальности имени, если оно не уникально.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        [Fact]
        public async Task ValidateNameUnique_ExistName_ValidationFailed()
        {
            Func<Task> act = () => validator.ValidateNameUnique(existName, deviceRepositoryMock.Object);

            await Assert.ThrowsAsync<MonitoringException>(act);
            deviceRepositoryMock.Verify(x => x.IsNameUnique(existName), Times.Once);
        }

        /// <summary>
        /// Тест проверки уникальности имени, если оно уникально.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        [Fact]
        public async Task ValidateNameUnique_NewName_Validated()
        {
            await validator.ValidateNameUnique(newName, deviceRepositoryMock.Object);

            deviceRepositoryMock.Verify(x => x.IsNameUnique(newName), Times.Once);
        }
    }
}
