﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Monitoring.Mvc.Migrations;
using Monitoring.Mvc.Models;
using Monitoring.Mvc.Repositories;
using Monitoring.Mvc.Uow;
using Xunit;

namespace Monitoring.Tests
{
    /// <summary>
    /// Тесты для класса NHibernateDeviceEventRepository.
    /// </summary>
    public class NHibernateDeviceEventRepositoryTests
    {
        private readonly IUnitOfWorkFactory unitOfWorkFactory;

        /// <summary>
        /// Конструктор.
        /// </summary>
        public NHibernateDeviceEventRepositoryTests()
        {
            var dic = new Dictionary<string, string>
            {
                ["UseNHibernate"] = "true"
            };

            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("testsettings.json")
                .AddInMemoryCollection(dic)
                .Build();

            var connectionString = config["ConnectionString"];
            DbMigrator.StartMigration(connectionString);

            unitOfWorkFactory = new UnitOfWorkFactory(config);
        }

        /// <summary>
        /// Тест добавления события устройства в БД.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        [Fact]
        public async Task Add_AddDeviceEventToRepository_DeviceEventAdded()
        {
            Guid deviceId = Guid.NewGuid();
            Device device = CreateDevice(deviceId);
            DeviceEvent deviceEvent = CreateDeviceEventForDevice(deviceId);

            using (var unitOfWork = unitOfWorkFactory.Create())
            {
                var deviceRepository = unitOfWork.GetDeviceRepository();
                var deviceEventRepository = unitOfWork.GetDeviceEventRepository();
                await deviceRepository.Add(device);
                await deviceEventRepository.Add(deviceEvent);

                DeviceEvent result = await deviceEventRepository.Get(deviceEvent.Id);

                Assert.IsType<NHibernateDeviceRepository>(deviceRepository);
                Assert.IsType<NHibernateDeviceEventRepository>(deviceEventRepository);
                Assert.Equal(deviceEvent.Name, result.Name);
                Assert.Equal(deviceEvent.Date.ToString(), result.Date.ToString()); // Чтобы отбросить миллисекунды
                Assert.Equal(deviceEvent.DeviceId, result.DeviceId);
            }
        }

        /// <summary>
        /// Тест удаления списка событий устройства из БД по идентификатору устройства.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        [Fact]
        public async Task DeleteForDevice_RemoveDeviceEventListForDevice_DeviceEventsRemoved()
        {
            Guid deviceId = Guid.NewGuid();
            Device device = CreateDevice(deviceId);
            DeviceEvent deviceEvent = CreateDeviceEventForDevice(deviceId);

            using (var unitOfWork = unitOfWorkFactory.Create())
            {
                var deviceRepository = unitOfWork.GetDeviceRepository();
                var deviceEventRepository = unitOfWork.GetDeviceEventRepository();
                await deviceRepository.Add(device);
                await deviceEventRepository.Add(deviceEvent);

                await deviceEventRepository.DeleteForDevice(deviceId);

                List<DeviceEvent> result = await deviceEventRepository.GetForDevice(deviceId);

                Assert.IsType<NHibernateDeviceRepository>(deviceRepository);
                Assert.IsType<NHibernateDeviceEventRepository>(deviceEventRepository);
                Assert.Empty(result);
            }
        }

        /// <summary>
        /// Тест получения списка событий устройства из БД по идентификатору устройства.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        [Fact]
        public async Task GetForDevice_DeviceEventListForDeviceRequest_DeviceListReturned()
        {
            Guid deviceId = Guid.NewGuid();
            Device device = CreateDevice(deviceId);
            DeviceEvent deviceEvent1 = CreateDeviceEventForDevice(deviceId);
            DeviceEvent deviceEvent2 = CreateDeviceEventForDevice(deviceId);

            using (var unitOfWork = unitOfWorkFactory.Create())
            {
                var deviceRepository = unitOfWork.GetDeviceRepository();
                var deviceEventRepository = unitOfWork.GetDeviceEventRepository();
                await deviceRepository.Add(device);
                await deviceEventRepository.Add(deviceEvent1);
                await deviceEventRepository.Add(deviceEvent2);

                List<DeviceEvent> result = await deviceEventRepository.GetForDevice(deviceId);

                Assert.IsType<NHibernateDeviceRepository>(deviceRepository);
                Assert.IsType<NHibernateDeviceEventRepository>(deviceEventRepository);
                Assert.Equal(2, result.Count);
                Assert.All(result, item => Assert.IsType<DeviceEvent>(item));
                Assert.All(result, item => Assert.Equal(deviceId, item.DeviceId));
            }
        }

        private Device CreateDevice(Guid id)
        {
            var device = new Device
            {
                Id = id,
                Name = $"Test Node {id}",
                LastStatisticDate = DateTime.Today,
                DateReceived = DateTime.Now,
                Version = "1.1.0.0",
                OperationSystem = "Win 10x"
            };

            return device;
        }

        private DeviceEvent CreateDeviceEventForDevice(Guid deviceId)
        {
            Guid id = Guid.NewGuid();
            var device = new DeviceEvent
            {
                Id = id,
                Name = $"Test Event {id}",
                Date = DateTime.Today,
                DeviceId = deviceId
            };

            return device;
        }
    }
}
