﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Monitoring.Mvc.Mediator.Handlers;
using Monitoring.Mvc.Mediator.Requests;
using Monitoring.Mvc.Models;
using Monitoring.Mvc.Repositories;
using Moq;
using Xunit;

namespace Monitoring.Tests
{
    /// <summary>
    /// Тесты для класса UpdateDeviceNameHandler.
    /// </summary>
    public class UpdateDeviceNameHandlerTests
    {
        /// <summary>
        /// Тест обработки запроса, если передан пустой объект репозитория.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        [Fact]
        public async Task Handle_NullRepository_NameNotUpdated()
        {
            var updateDeviceNameHandler = new UpdateDeviceNameHandler();
            var request = new UpdateDeviceNameMediatorRequest();

            Task Action() => updateDeviceNameHandler.Handle(request, CancellationToken.None);

            await Assert.ThrowsAsync<ApplicationException>(Action);
        }

        /// <summary>
        /// Тест обработки запроса, если передан непустой объект репозитория.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        [Fact]
        public async Task Handle_RepositoryExist_NameUpdated()
        {
            var updateDeviceNameHandler = new UpdateDeviceNameHandler();
            var deviceRepositoryMock = new Mock<IDeviceRepository>();
            deviceRepositoryMock.Setup(dr => dr.Get(It.IsAny<Guid>()))
                .ReturnsAsync(new Device());
            var request = new UpdateDeviceNameMediatorRequest
            {
                DeviceRepository = deviceRepositoryMock.Object
            };

            await updateDeviceNameHandler.Handle(request, CancellationToken.None);

            deviceRepositoryMock.Verify(dr => dr.Update(It.IsAny<Device>()));
        }
    }
}
