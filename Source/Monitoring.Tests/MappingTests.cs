﻿using System;
using Mapster;
using Monitoring.Contracts;
using Monitoring.Contracts.ProtoBuf;
using Monitoring.Mvc;
using Monitoring.Mvc.Mediator.Requests;
using Monitoring.Mvc.Models;
using Xunit;

namespace Monitoring.Tests
{
    /// <summary>
    /// Тесты на маппинг объектов.
    /// </summary>
    public class MappingTests
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MappingTests"/> class.
        /// </summary>
        public MappingTests()
        {
            Startup.ConfigureMapping();
        }

        /// <summary>
        /// Тест маппинга <see cref="Device"/> в <see cref="DeviceResponse"/>.
        /// </summary>
        [Fact]
        public void Device_to_DeviceResponse()
        {
            var source = new Device
            {
                Id = Guid.NewGuid(),
                Name = "Device name",
                OperationSystem = "Device os",
                Version = "Device version",
                LastStatisticDate = DateTime.Now,
                DateReceived = DateTime.Now,
                IsUserDefinedName = false
            };

            var dest = source.Adapt<DeviceResponse>();

            Assert.Equal(source.Id, dest.Id);
            Assert.Equal(source.Name, dest.Name);
            Assert.Equal(source.OperationSystem, dest.OperationSystem);
            Assert.Equal(source.Version, dest.Version);
            Assert.Equal(source.LastStatisticDate, dest.LastStatisticDate);
        }

        /// <summary>
        /// Тест маппинга <see cref="DeviceEvent"/> в <see cref="DeviceEventResponse"/>.
        /// </summary>
        [Fact]
        public void DeviceEvent_to_DeviceEventResponse()
        {
            var source = new DeviceEvent
            {
                Id = Guid.NewGuid(),
                DeviceId = Guid.NewGuid(),
                Date = DateTime.Now,
                Level = DeviceEventLevel.Low,
                Name = "Test Event"
            };

            var dest = source.Adapt<DeviceEventResponse>();

            Assert.Equal(source.Name, dest.Name);
            Assert.Equal(source.Date, dest.Date);
            Assert.Equal(source.Level, dest.Level);
        }

        /// <summary>
        /// Тест маппинга <see cref="DeviceEventRequest"/> в <see cref="DeviceEvent"/>.
        /// </summary>
        [Fact]
        public void DeviceEventRequest_to_DeviceEvent()
        {
            var source = new DeviceEventRequest
            {
                Date = DateTime.Now,
                Level = DeviceEventLevel.Low,
                Name = "Test Event"
            };

            var dest = source.Adapt<DeviceEvent>();

            Assert.Equal(source.Name, dest.Name);
            Assert.Equal(source.Date, dest.Date);
            Assert.Equal(source.Level, dest.Level);
        }

        /// <summary>
        /// Тест маппинга <see cref="DeviceEventRequest"/> в <see cref="NotifyDeviceEventProtoBuf"/>.
        /// </summary>
        [Fact]
        public void DeviceEventRequest_to_NotifyDeviceEventProtoBuf()
        {
            var source = new DeviceEventRequest
            {
                Date = DateTime.Now,
                Level = DeviceEventLevel.Low,
                Name = "Test Event"
            };

            var dest = source.Adapt<NotifyDeviceEventProtoBuf>();

            Assert.Equal(source.Name, dest.Name);
            Assert.Equal(source.Date, dest.Date);
            Assert.Equal(source.Level, dest.Level);
        }

        /// <summary>
        /// Тест маппинга <see cref="DeviceRequest"/> в <see cref="Device"/>.
        /// </summary>
        [Fact]
        public void DeviceRequest_to_Device()
        {
            var source = new DeviceRequest
            {
                Id = Guid.NewGuid(),
                Name = "Device name",
                Os = "Device os",
                Version = "Device version",
                LastStatisticDate = DateTime.Now.ToString()
            };

            var dest = source.Adapt<Device>();

            Assert.Equal(source.Id, dest.Id);
            Assert.Equal(source.Name, dest.Name);
            Assert.Equal(source.Os, dest.OperationSystem);
            Assert.Equal(source.Version, dest.Version);
            Assert.Equal(source.LastStatisticDate, dest.LastStatisticDate.ToString());
        }

        /// <summary>
        /// Тест маппинга <see cref="UpdateDeviceNameRequest"/> в <see cref="UpdateDeviceNameMediatorRequest"/>.
        /// </summary>
        [Fact]
        public void UpdateDeviceNameRequest_to_UpdateDeviceNameMediatorRequest()
        {
            var source = new UpdateDeviceNameRequest
            {
                DeviceId = Guid.NewGuid(),
                Name = "Device name"
            };

            var dest = source.Adapt<UpdateDeviceNameMediatorRequest>();

            Assert.Equal(source.DeviceId, dest.DeviceId);
            Assert.Equal(source.Name, dest.Name);
        }
    }
}
