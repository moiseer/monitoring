﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Monitoring.Contracts.ProtoBuf;
using ProtoBuf;
using RabbitMQ.Client;

namespace Monitoring.Mvc.MessageSender
{
    /// <summary>
    /// Отправитель сообщений в службу брокера сообщений RabbitMQ.
    /// </summary>
    public class RabbitSender : IMessageSender
    {
        private readonly IModel channel;
        private readonly IConnection connection;
        private readonly string exchangeName;
        private readonly string routingKey;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="configuration">Конфигурация подключеиня к службе брокера сообщений.</param>
        public RabbitSender(IConfiguration configuration)
        {
            string connectionString = configuration["RabbitConnectionString"];
            exchangeName = configuration["ExchangeName"];
            string queueName = configuration["QueueName"];
            routingKey = configuration["RoutingKey"];

            connection = GetRabbitConnection(connectionString);
            channel = GetRabbitChannel(connection, exchangeName, queueName, routingKey);
        }

        /// <summary>
        /// Закрытие подключения к каналу и подключения к службе брокера сообщений.
        /// </summary>
        public void Dispose()
        {
            channel?.Close();
            channel?.Dispose();
            connection?.Close();
            connection?.Dispose();
        }

        /// <summary>
        /// Отправка сообщения в систему обмена сообщениями.
        /// </summary>
        /// <param name="response">Отправляемый объект.</param>
        public void Send(IProtoBufContract response)
        {
            byte[] message;
            using (var stream = new MemoryStream())
            {
                Serializer.Serialize(stream, response);
                message = stream.ToArray();
            }

            channel.BasicPublish(exchangeName, routingKey, null, message);
        }

        private IModel GetRabbitChannel(IConnection connection, string exchangeName, string queueName, string routingKey)
        {
            IModel channel = connection.CreateModel();
            channel.ExchangeDeclare(exchangeName, ExchangeType.Direct);
            channel.QueueDeclare(queueName, false, false, false, null);
            channel.QueueBind(queueName, exchangeName, routingKey, null);
            return channel;
        }

        private IConnection GetRabbitConnection(string connectionString)
        {
            ConnectionFactory factory = new ConnectionFactory
            {
                Uri = new Uri(connectionString)
            };

            return factory.CreateConnection();
        }
    }
}
