﻿using System;
using Monitoring.Contracts.ProtoBuf;

namespace Monitoring.Mvc.MessageSender
{
    /// <summary>
    /// Интерфейс отправителя сообщений в систему обмена сообщениями.
    /// </summary>
    public interface IMessageSender : IDisposable
    {
        /// <summary>
        /// Отправка сообщения в систему обмена сообщениями.
        /// </summary>
        /// <param name="response">Отправляемый объект.</param>
        void Send(IProtoBufContract response);
    }
}
