﻿using System;
using System.Collections.Generic;
using Monitoring.Contracts.ProtoBuf;

namespace Monitoring.Mvc.Services
{
    /// <summary>
    /// Интерфейс сервиса для работы с сообщениями.
    /// </summary>
    public interface IMessageService
    {
        /// <summary>
        /// Отправка оповещений о событиях с высоким уровнем критичности.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства, которое является источником событий.</param>
        /// <param name="events">Список событий.</param>
        void NotifyAboutHighLevelEvents(Guid deviceId, IEnumerable<NotifyDeviceEventProtoBuf> events);
    }
}
