﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Monitoring.Contracts.SignalR;
using Monitoring.Mvc.SignalR.Hubs;

namespace Monitoring.Mvc.Services
{
    /// <summary>
    /// Cервис оповещения клиентов.
    /// </summary>
    public class ClientNotifyService : IClientNotifyService
    {
        private readonly IHubContext<ClientHub> clientHub;

        /// <summary>
        /// Констпуктор.
        /// </summary>
        /// <param name="clientHub">
        /// Концентратор, который служит в качестве конвейера высокого уровня
        /// для обработки взаимодействия между клиентом и сервером.
        /// </param>
        public ClientNotifyService(IHubContext<ClientHub> clientHub)
        {
            this.clientHub = clientHub;
        }

        /// <summary>
        /// Получение клиентом оповещения о событии.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <param name="deviceEventType">Тип события.</param>
        /// <returns>Завершенная задача.</returns>
        public async Task Notify(Guid deviceId, ServerDeviceEventType deviceEventType)
        {
            var deviceEvent = new ServerDeviceEvent
            {
                DeviceId = deviceId,
                Type = deviceEventType
            };
            await clientHub.Clients.All.SendAsync("ServerEvent", deviceEvent);
        }
    }
}
