﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Mapster;
using MediatR;
using Monitoring.Contracts;
using Monitoring.Contracts.ProtoBuf;
using Monitoring.Contracts.SignalR;
using Monitoring.Mvc.Core;
using Monitoring.Mvc.Mediator.Requests;
using Monitoring.Mvc.Models;
using Monitoring.Mvc.Repositories;
using Monitoring.Mvc.Uow;
using Monitoring.Mvc.Validators;
using Serilog;

namespace Monitoring.Mvc.Services
{
    /// <summary>
    /// Сервис для работы с данными устройств.
    /// </summary>
    public class DeviceService : IDeviceService
    {
        private readonly IClientNotifyService clientNotifyService;
        private readonly ILogger logger = Log.ForContext<DeviceService>();
        private readonly IMediator mediator;
        private readonly IMessageService messageService;
        private readonly IUnitOfWorkFactory unitOfWorkFactory;
        private readonly IDeviceValidator validator;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="unitOfWorkFactory">Фабрика объектов UnitOfWork.</param>
        /// <param name="validator">Валидатор устройств.</param>
        /// <param name="mediator">Объект-посредник, выполняющий передачу запросов в соответствующий обоаботчик.</param>
        /// <param name="messageService">Сервис для работы с сообщениями.</param>
        /// <param name="clientNotifyService">Cервис оповещения клиентов.</param>
        public DeviceService(
            IUnitOfWorkFactory unitOfWorkFactory,
            IDeviceValidator validator,
            IMediator mediator,
            IMessageService messageService,
            IClientNotifyService clientNotifyService)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
            this.validator = validator;
            this.mediator = mediator;
            this.messageService = messageService;
            this.clientNotifyService = clientNotifyService;
        }

        /// <summary>
        /// Добавление или обновление данных об устройстве.
        /// </summary>
        /// <param name="device">Данные об устройстве.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task AddOrUpdate(DeviceRequest device)
        {
            validator.Validate(device);

            using (IUnitOfWork uow = unitOfWorkFactory.Create())
            {
                IDeviceRepository deviceRepository = uow.GetDeviceRepository();
                IDeviceEventRepository deviceEventRepository = uow.GetDeviceEventRepository();

                await AddOrUpdateDevice(deviceRepository, device.Adapt<Device>());

                messageService.NotifyAboutHighLevelEvents(device.Id, device.Events.Adapt<List<NotifyDeviceEventProtoBuf>>());
                await AddDeviceEvents(deviceEventRepository, device.Id, device.Events.Adapt<List<DeviceEvent>>());

                uow.Commit();
            }

            logger.Information("Данные об устройстве {Id} обновлены.", device.Id);
            await clientNotifyService.Notify(device.Id, ServerDeviceEventType.DeviceUpdated);
            if (device.Events.Any())
            {
                logger.Information("События устройства {Id} добавлены.", device.Id);
                await clientNotifyService.Notify(device.Id, ServerDeviceEventType.DeviceEventsUpdated);
            }
        }

        /// <summary>
        /// Проверка уникальности имени узла устройства.
        /// </summary>
        /// <param name="name">Имя узла.</param>
        /// <returns>Уникальность имени узла.</returns>
        public async Task<bool> CheckDeviceNameUnique(string name)
        {
            using (IUnitOfWork uow = unitOfWorkFactory.Create())
            {
                IDeviceRepository deviceRepository = uow.GetDeviceRepository();

                return await deviceRepository.IsNameUnique(name);
            }
        }

        /// <summary>
        /// Удаление данных об устройстве.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task Delete(Guid deviceId)
        {
            using (IUnitOfWork uow = unitOfWorkFactory.Create())
            {
                IDeviceRepository deviceRepository = uow.GetDeviceRepository();
                IDeviceEventRepository deviceEventRepository = uow.GetDeviceEventRepository();

                var item = await deviceRepository.Get(deviceId);

                item.ToOption()
                    .DoOnEmpty(() => logger.Error("Данные устройства с Id {Id} не найдены.", deviceId))
                    .ThrowOnEmpty(() => new MonitoringException("Запись не найдена.", HttpStatusCode.NotFound));

                await deviceEventRepository.DeleteForDevice(deviceId);
                await deviceRepository.Delete(deviceId);

                uow.Commit();
            }

            logger.Information("Данные об устройстве {Id} удалены.", deviceId);
            await clientNotifyService.Notify(deviceId, ServerDeviceEventType.DeviceDeleted);
        }

        /// <summary>
        /// Удаление списка событий устройства по идентификатору устройства.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task DeleteEventsForDevice(Guid deviceId)
        {
            using (IUnitOfWork uow = unitOfWorkFactory.Create())
            {
                IDeviceEventRepository deviceEventRepository = uow.GetDeviceEventRepository();

                if (await deviceEventRepository.CountForDevice(deviceId) <= 0)
                {
                    return;
                }

                await deviceEventRepository.DeleteForDevice(deviceId);

                uow.Commit();
            }

            logger.Information("Список событий устройства {Id} удален.", deviceId);
            await clientNotifyService.Notify(deviceId, ServerDeviceEventType.DeviceEventsUpdated);
        }

        /// <summary>
        /// Получение списка данных о всех устройствах.
        /// </summary>
        /// <returns>Список данных о всех устройствах.</returns>
        public async Task<List<DeviceResponse>> Get()
        {
            using (IUnitOfWork uow = unitOfWorkFactory.Create())
            {
                IDeviceRepository deviceRepository = uow.GetDeviceRepository();

                List<Device> items = await deviceRepository.Get();

                return items.Adapt<List<DeviceResponse>>();
            }
        }

        /// <summary>
        /// Получение данных об устройстве по идентификатору.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Данные об устройстве.</returns>
        public async Task<DeviceResponse> Get(Guid deviceId)
        {
            using (IUnitOfWork uow = unitOfWorkFactory.Create())
            {
                IDeviceRepository deviceRepository = uow.GetDeviceRepository();

                var item = await deviceRepository.Get(deviceId);

                item.ToOption()
                    .DoOnEmpty(() => logger.Error("Данные устройства с Id {Id} не найдены.", deviceId))
                    .ThrowOnEmpty(() => new MonitoringException("Запись не найдена.", HttpStatusCode.NotFound));

                return item.Adapt<DeviceResponse>();
            }
        }

        /// <summary>
        /// Получение списка событий устройства по идентификатору.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Список событий устройства.</returns>
        public async Task<DeviceEventsResponse> GetEventsForDevice(Guid deviceId)
        {
            using (IUnitOfWork uow = unitOfWorkFactory.Create())
            {
                IDeviceEventRepository deviceEventRepository = uow.GetDeviceEventRepository();

                List<DeviceEvent> items = await deviceEventRepository.GetForDevice(deviceId);

                return new DeviceEventsResponse
                {
                    Events = items.Adapt<List<DeviceEventResponse>>()
                };
            }
        }

        /// <summary>
        /// Изменение имени узла устройства.
        /// </summary>
        /// <param name="request">Запрос на изменение имени узла.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task UpdateName(UpdateDeviceNameRequest request)
        {
            using (IUnitOfWork uow = unitOfWorkFactory.Create())
            {
                var updateDeviceName = request.Adapt<UpdateDeviceNameMediatorRequest>();
                updateDeviceName.DeviceRepository = uow.GetDeviceRepository();

                await mediator.Send(updateDeviceName);

                uow.Commit();
            }

            logger.Information("Имя узла устройства устройства {Id} изменено на {Name}.", request.DeviceId, request.Name);
            await clientNotifyService.Notify(request.DeviceId, ServerDeviceEventType.DeviceUpdated);
        }

        private async Task AddDeviceEvents(IDeviceEventRepository deviceEventRepository, Guid deviceId, List<DeviceEvent> events)
        {
            foreach (DeviceEvent deviceEvent in events)
            {
                deviceEvent.DeviceId = deviceId;

                await deviceEventRepository.Add(deviceEvent);
            }
        }

        private async Task AddOrUpdateDevice(IDeviceRepository deviceRepository, Device device)
        {
            var item = await deviceRepository.Get(device.Id);

            if (item == null)
            {
                await deviceRepository.Add(device);
            }
            else
            {
                if (item.IsUserDefinedName)
                {
                    device.Name = item.Name;
                    device.IsUserDefinedName = item.IsUserDefinedName;
                }

                await deviceRepository.Update(device);
            }
        }
    }
}
