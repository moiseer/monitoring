﻿using System;
using System.Threading.Tasks;
using Monitoring.Contracts.SignalR;

namespace Monitoring.Mvc.Services
{
    /// <summary>
    /// Интерфейс сервиса оповещения клиентов.
    /// </summary>
    public interface IClientNotifyService
    {
        /// <summary>
        /// Получение клиентом оповещения о событии.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <param name="deviceEventType">Тип события.</param>
        /// <returns>Результат выполнения задачи.</returns>
        Task Notify(Guid deviceId, ServerDeviceEventType deviceEventType);
    }
}
