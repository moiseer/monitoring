﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Monitoring.Contracts;

namespace Monitoring.Mvc.Services
{
    /// <summary>
    /// Интерфейс сервиса для работы с данными устройств.
    /// </summary>
    public interface IDeviceService
    {
        /// <summary>
        /// Добавление или обновление данных об устройстве.
        /// </summary>
        /// <param name="device">Данные об устройстве.</param>
        /// <returns>Результат выполнения задачи.</returns>
        Task AddOrUpdate(DeviceRequest device);

        /// <summary>
        /// Проверка уникальности имени узла устройства.
        /// </summary>
        /// <param name="name">Имя узла.</param>
        /// <returns>Уникальность имени узла.</returns>
        Task<bool> CheckDeviceNameUnique(string name);

        /// <summary>
        /// Удаление данных об устройстве.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        Task Delete(Guid deviceId);

        /// <summary>
        /// Удаление списка событий устройства по идентификатору устройства.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        Task DeleteEventsForDevice(Guid deviceId);

        /// <summary>
        /// Получение списка данных о всех устройствах.
        /// </summary>
        /// <returns>Список данных о всех устройствах.</returns>
        Task<List<DeviceResponse>> Get();

        /// <summary>
        /// Получение данных об устройстве по идентификатору.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Данные об устройстве.</returns>
        Task<DeviceResponse> Get(Guid deviceId);

        /// <summary>
        /// Получение списка событий устройства по идентификатору.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Список событий устройства.</returns>
        Task<DeviceEventsResponse> GetEventsForDevice(Guid deviceId);

        /// <summary>
        /// Изменение имени узла устройства.
        /// </summary>
        /// <param name="request">Запрос на изменение имени узла.</param>
        /// <returns>Результат выполнения задачи.</returns>
        Task UpdateName(UpdateDeviceNameRequest request);
    }
}
