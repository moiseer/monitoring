﻿using System;
using System.Collections.Generic;
using System.Linq;
using Monitoring.Contracts;
using Monitoring.Contracts.ProtoBuf;
using Monitoring.Mvc.MessageSender;
using Serilog;

namespace Monitoring.Mvc.Services
{
    /// <summary>
    /// Сервис для работы с сообщениями.
    /// </summary>
    public class MessageService : IMessageService
    {
        private readonly ILogger logger = Log.ForContext<DeviceService>();
        private readonly IMessageSender messageSender;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="messageSender">Отправитель сообщений в систему обмена сообщениями.</param>
        public MessageService(IMessageSender messageSender)
        {
            this.messageSender = messageSender;
        }

        /// <summary>
        /// Отправка оповещений о событиях с высоким уровнем критичности.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства, которое является источником событий.</param>
        /// <param name="events">Список событий.</param>
        public void NotifyAboutHighLevelEvents(Guid deviceId, IEnumerable<NotifyDeviceEventProtoBuf> events)
        {
            var highLevelEvents = events.Where(e => e.Level == DeviceEventLevel.High);
            if (!highLevelEvents.Any())
            {
                return;
            }

            foreach (var deviceEvent in highLevelEvents)
            {
                deviceEvent.DeviceId = deviceId;

                logger.Information("Отправлено сообщение о событии с высоким уровнем критичности: {ServerEvent}", deviceEvent);
                messageSender.Send(deviceEvent);
            }
        }
    }
}
