﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Monitoring.Mvc.Core;
using Monitoring.Mvc.Mediator.Requests;
using Serilog;

namespace Monitoring.Mvc.Mediator.Handlers
{
    /// <summary>
    /// Обработчик запроса на изменение имени узла устройства.
    /// </summary>
    public class UpdateDeviceNameHandler : IRequestHandler<UpdateDeviceNameMediatorRequest>
    {
        private readonly ILogger logger = Log.ForContext<UpdateDeviceNameHandler>();

        /// <summary>
        /// Обработка запроса на изменение имени узла устройства.
        /// </summary>
        /// <param name="request">Запрос на изменение имени узла устройства.</param>
        /// <param name="cancellationToken">Токен отмены задачи.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task<Unit> Handle(UpdateDeviceNameMediatorRequest request, CancellationToken cancellationToken)
        {
            await request.DeviceRepository.ToOption()
                .DoOnEmpty(() => logger.Warning("Отсутствует ссылка на репозиторий."))
                .ThrowOnEmpty(() => new ApplicationException("Отсутствует ссылка на репозиторий."))
                .MapSync(deviceRepository => deviceRepository.Get(request.DeviceId))
                .DoOnEmpty(() => logger.Error("Данные устройства с Id {Id} не найдены.", request.DeviceId))
                .ThrowOnEmpty(() => new MonitoringException("Запись не найдена.", HttpStatusCode.NotFound))
                .Do(item => item.Name = request.Name)
                .Do(item => item.IsUserDefinedName = true)
                .DoAsync(item => request.DeviceRepository.Update(item));

            return Unit.Value;
        }
    }
}
