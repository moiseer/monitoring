﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Monitoring.Mvc.Mediator.Requests;
using Monitoring.Mvc.Validators;

namespace Monitoring.Mvc.Mediator.Behaviors
{
    /// <summary>
    /// Валидатор имени узла устройства, вызываемый в MediatR.
    /// </summary>
    public class ValidateDeviceNameBehaviour : IPipelineBehavior<UpdateDeviceNameMediatorRequest, Unit>
    {
        private readonly IDeviceValidator validator;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="validator">Валидатор устройства.</param>
        public ValidateDeviceNameBehaviour(IDeviceValidator validator)
        {
            this.validator = validator;
        }

        /// <summary>
        /// Валидация имени узла устройства.
        /// </summary>
        /// <param name="request">ЗАпрос на изменение имени.</param>
        /// <param name="cancellationToken">Токен отмены.</param>
        /// <param name="next">Делегат изменения имени узла устройства.</param>
        /// <returns>Результат измения имени узла устройства.</returns>
        public async Task<Unit> Handle(UpdateDeviceNameMediatorRequest request,
            CancellationToken cancellationToken,
            RequestHandlerDelegate<Unit> next)
        {
            validator.ValidateNameLength(request.DeviceId, request.Name);
            await validator.ValidateNameUnique(request);

            return await next();
        }
    }
}
