﻿using System;
using MediatR;
using Monitoring.Mvc.Repositories;

namespace Monitoring.Mvc.Mediator.Requests
{
    /// <summary>
    /// Запрос на изменение имени устройства для медиатора.
    /// </summary>
    public class UpdateDeviceNameMediatorRequest : IRequest
    {
        /// <summary>
        /// Идентификатор устройства.
        /// </summary>
        public Guid DeviceId { get; set; }

        /// <summary>
        /// Репозиторий данных об устройствах.
        /// </summary>
        public IDeviceRepository DeviceRepository { get; set; }

        /// <summary>
        /// Новое имя узла устройства.
        /// </summary>
        public string Name { get; set; }
    }
}
