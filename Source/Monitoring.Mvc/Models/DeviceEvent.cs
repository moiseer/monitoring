﻿using System;
using Monitoring.Contracts;

namespace Monitoring.Mvc.Models
{
    /// <summary>
    /// Событие устройства.
    /// </summary>
    public class DeviceEvent
    {
        /// <summary>
        /// Дата и время возникновения.
        /// </summary>
        public virtual DateTime Date { get; set; }

        /// <summary>
        /// Идентификатор устройства.
        /// </summary>
        public virtual Guid DeviceId { get; set; }

        /// <summary>
        /// Идентификатор события.
        /// </summary>
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Уровень критичности.
        /// </summary>
        public virtual DeviceEventLevel Level { get; set; }

        /// <summary>
        /// Название.
        /// </summary>
        public virtual string Name { get; set; }
    }
}
