﻿using System;

namespace Monitoring.Mvc.Models
{
    /// <summary>
    /// Данные об устройстве.
    /// </summary>
    public class Device
    {
        /// <summary>
        /// Дата и время получения статистики.
        /// </summary>
        public virtual DateTime DateReceived { get; set; }

        /// <summary>
        /// Идентификатор.
        /// </summary>
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Флаг, указывающий было ли имя узла изменено через пользовательский интерфейс.
        /// </summary>
        public virtual bool IsUserDefinedName { get; set; }

        /// <summary>
        /// Дата и время статистики.
        /// </summary>
        public virtual DateTime LastStatisticDate { get; set; }

        /// <summary>
        /// Имя узла.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Тип устройства, версия операционной системы.
        /// </summary>
        public virtual string OperationSystem { get; set; }

        /// <summary>
        /// Версия приложения.
        /// </summary>
        public virtual string Version { get; set; }
    }
}
