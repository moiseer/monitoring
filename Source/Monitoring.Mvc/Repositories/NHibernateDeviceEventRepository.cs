﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Monitoring.Mvc.Models;
using NHibernate;
using NHibernate.Linq;

namespace Monitoring.Mvc.Repositories
{
    /// <summary>
    /// Репозиторий событий устройств.
    /// </summary>
    public class NHibernateDeviceEventRepository : IDeviceEventRepository
    {
        private readonly ISession session;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="session">Соединение с БД.</param>
        public NHibernateDeviceEventRepository(ISession session)
        {
            this.session = session;
        }

        /// <summary>
        /// Добавление события устройства в БД.
        /// </summary>
        /// <param name="deviceEvent">Событие устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task Add(DeviceEvent deviceEvent)
        {
            await session.SaveAsync(deviceEvent);
        }

        /// <summary>
        /// Получение количества событий устройства в БД по идентификатору устройства.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task<int> CountForDevice(Guid deviceId)
        {
            return await session.Query<DeviceEvent>()
                .CountAsync(deviceEvent => deviceEvent.DeviceId == deviceId);
        }

        /// <summary>
        /// Удаление события устройства из БД по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор события устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task Delete(Guid id)
        {
            var deviceEvent = await session.GetAsync<DeviceEvent>(id);
            await session.DeleteAsync(deviceEvent);
        }

        /// <summary>
        /// Удаление всех событий устройств из БД.
        /// </summary>
        /// <returns>
        /// <placeholder>A <see cref="Task"/> representing the asynchronous operation.</placeholder>
        /// </returns>
        public async Task DeleteAll()
        {
            await session.Query<DeviceEvent>().DeleteAsync(default);
        }

        /// <summary>
        /// Удаление списка событий устройства из БД по идентификатору устройства.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task DeleteForDevice(Guid deviceId)
        {
            await session.Query<DeviceEvent>()
                .Where(deviceEvent => deviceEvent.DeviceId == deviceId)
                .DeleteAsync(default);
        }

        /// <summary>
        /// Получение списка событий устройств из БД.
        /// </summary>
        /// <returns>Список событий устройств.</returns>
        public async Task<List<DeviceEvent>> Get()
        {
            return await session.Query<DeviceEvent>().ToListAsync();
        }

        /// <summary>
        /// Получение события устройства из БД по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор события устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task<DeviceEvent> Get(Guid id)
        {
            return await session.GetAsync<DeviceEvent>(id);
        }

        /// <summary>
        /// олучение списка событий устройства из БД по идентификатору устройства.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Список событий устройства.</returns>
        public async Task<List<DeviceEvent>> GetForDevice(Guid deviceId)
        {
            return await session.Query<DeviceEvent>()
                .Where(deviceEvent => deviceEvent.DeviceId == deviceId)
                .ToListAsync();
        }

        /// <summary>
        /// Обносление события устройства в БД.
        /// </summary>
        /// <param name="deviceEvent">Событие устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task Update(DeviceEvent deviceEvent)
        {
            await session.UpdateAsync(deviceEvent);
        }
    }
}
