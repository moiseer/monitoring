﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Monitoring.Mvc.Models;

namespace Monitoring.Mvc.Repositories
{
    /// <summary>
    /// Интерфейс репозитория данных об устройствах.
    /// </summary>
    public interface IDeviceRepository
    {
        /// <summary>
        /// Добавление данных об устройстве в репозиторий.
        /// </summary>
        /// <param name="device">Данные об устройстве.</param>
        /// <returns>Результат выполнения задачи.</returns>
        Task Add(Device device);

        /// <summary>
        /// Удаление данных об устройстве из репозитория.
        /// </summary>
        /// <param name="id">Идентификатор устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        Task Delete(Guid id);

        /// <summary>
        /// Удаление данных о всех устройствах из репозитория.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        Task DeleteAll();

        /// <summary>
        /// Получение списка данных о всех устройствах из репозитория.
        /// </summary>
        /// <returns>Список данных о всех устройствах.</returns>
        Task<List<Device>> Get();

        /// <summary>
        /// Получение данных об устройстве из репозитория по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор устройства.</param>
        /// <returns>Данные об устройстве.</returns>
        Task<Device> Get(Guid id);

        /// <summary>
        /// Проверка уникальности имени узла устройства в репозитории.
        /// </summary>
        /// <param name="name">Имя узла.</param>
        /// <returns>Уникальность имени узла.</returns>
        Task<bool> IsNameUnique(string name);

        /// <summary>
        /// Обновление данных об устройстве в репозитории.
        /// </summary>
        /// <param name="device">Данные об устройстве.</param>
        /// <returns>Результат выполнения задачи.</returns>
        Task Update(Device device);
    }
}
