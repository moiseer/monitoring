﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Monitoring.Mvc.Models;

namespace Monitoring.Mvc.Repositories
{
    /// <summary>
    /// Репозиторий данных об устройствах.
    /// </summary>
    public class DeviceRepository : IDeviceRepository
    {
        private readonly IDbConnection connection;
        private readonly IDbTransaction transaction;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="connection">Соединение с БД.</param>
        /// <param name="transaction">Транзакция.</param>
        public DeviceRepository(IDbConnection connection, IDbTransaction transaction)
        {
            this.connection = connection;
            this.transaction = transaction;

            DefaultTypeMap.MatchNamesWithUnderscores = true;
        }

        /// <summary>
        /// Добавление данных об устройстве в БД.
        /// </summary>
        /// <param name="device">Данные об устройстве.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task Add(Device device)
        {
            await connection.ExecuteAsync(
                @"
                INSERT INTO device 
                (id, name, last_statistic_date, date_received, operation_system, version, is_user_defined_name)
                VALUES 
                (@Id, @Name, @LastStatisticDate, @DateReceived, @OperationSystem, @Version, @IsUserDefinedName)
                ",
                device,
                transaction);
        }

        /// <summary>
        /// Удаление данных об устройстве из БД.
        /// </summary>
        /// <param name="id">Идентификатор устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task Delete(Guid id)
        {
            await connection.ExecuteAsync(
                @"DELETE FROM device WHERE id = @Id",
                new { Id = id },
                transaction);
        }

        /// <summary>
        /// Удаление данных о всех устройствах из БД.
        /// </summary>
        /// <returns>
        /// <placeholder>A <see cref="Task"/> representing the asynchronous operation.</placeholder>
        /// </returns>
        public async Task DeleteAll()
        {
            await connection.ExecuteAsync(
                @"DELETE FROM device",
                transaction);
        }

        /// <summary>
        /// Получение списка данных о всех устройствах из БД.
        /// </summary>
        /// <returns>Список данных о всех устройствах.</returns>
        public async Task<List<Device>> Get()
        {
            IEnumerable<Device> devices = await connection.QueryAsync<Device>(
                @"
                SELECT
                id, name, last_statistic_date, date_received, operation_system, version, is_user_defined_name
                FROM device
                ",
                transaction);

            return devices.ToList();
        }

        /// <summary>
        /// Получение данных об устройстве из БД по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор устройства.</param>
        /// <returns>Данные об устройстве.</returns>
        public async Task<Device> Get(Guid id)
        {
            return await connection.QueryFirstOrDefaultAsync<Device>(
                @"
                    SELECT
                    id, name, last_statistic_date, date_received, operation_system, version, is_user_defined_name
                    FROM device 
                    WHERE id = @Id
                    ",
                new { Id = id },
                transaction);
        }

        /// <summary>
        /// Проверка уникальности имени узла устройства в БД.
        /// </summary>
        /// <param name="name">Имя узла.</param>
        /// <returns>Уникальность имени узла.</returns>
        public async Task<bool> IsNameUnique(string name)
        {
            return !await connection.ExecuteScalarAsync<bool>(
                @"
                SELECT CAST(COUNT(name) as BIT) 
                FROM device
                WHERE name = @Name
                ",
                new { Name = name },
                transaction
            );
        }

        /// <summary>
        /// Обновление данных об устройстве в БД.
        /// </summary>
        /// <param name="device">Данные об устройстве.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task Update(Device device)
        {
            await connection.ExecuteAsync(
                @"
                UPDATE device SET 
                name = @Name, 
                last_statistic_date = @LastStatisticDate,
                date_received = @DateReceived,
                operation_system = @OperationSystem,
                version = @Version,
                is_user_defined_name = @IsUserDefinedName
                WHERE id = @Id
                ",
                device,
                transaction);
        }
    }
}
