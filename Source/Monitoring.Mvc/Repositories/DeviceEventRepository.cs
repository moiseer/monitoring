﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Monitoring.Mvc.Models;

namespace Monitoring.Mvc.Repositories
{
    /// <summary>
    /// Репозиторий событий устройств.
    /// </summary>
    public class DeviceEventRepository : IDeviceEventRepository
    {
        private readonly IDbConnection connection;
        private readonly IDbTransaction transaction;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="connection">Соединение с БД.</param>
        /// <param name="transaction">Транзакция.</param>
        public DeviceEventRepository(IDbConnection connection, IDbTransaction transaction)
        {
            this.connection = connection;
            this.transaction = transaction;

            DefaultTypeMap.MatchNamesWithUnderscores = true;
        }

        /// <summary>
        /// Добавление события устройства в БД.
        /// </summary>
        /// <param name="deviceEvent">Событие устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task Add(DeviceEvent deviceEvent)
        {
            await connection.ExecuteAsync(
                @"
                INSERT INTO device_event 
                (id, name, date, device_id, level)
                VALUES 
                (@Id, @Name, @Date, @DeviceId, @Level)
                ",
                deviceEvent,
                transaction);
        }

        /// <summary>
        /// Получение количества событий устройства в БД по идентификатору устройства.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task<int> CountForDevice(Guid deviceId)
        {
            return await connection.ExecuteScalarAsync<int>(
                @"
                SELECT
                count(id)
                FROM device_event
                WHERE device_id = @DeviceId
                ",
                new { DeviceId = deviceId },
                transaction);
        }

        /// <summary>
        /// Удаление события устройства из БД по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор события устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task Delete(Guid id)
        {
            await connection.ExecuteAsync(
                @"DELETE FROM device_event WHERE id = @Id",
                new { Id = id },
                transaction);
        }

        /// <summary>
        /// Удаление всех событий устройств из БД.
        /// </summary>
        /// <returns>
        /// <placeholder>A <see cref="Task"/> representing the asynchronous operation.</placeholder>
        /// </returns>
        public async Task DeleteAll()
        {
            await connection.ExecuteAsync(
                @"DELETE FROM device_event",
                transaction);
        }

        /// <summary>
        /// Удаление списка событий устройства из БД по идентификатору устройства.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task DeleteForDevice(Guid deviceId)
        {
            await connection.ExecuteAsync(
                @"DELETE FROM device_event WHERE device_id = @DeviceId",
                new { DeviceId = deviceId },
                transaction);
        }

        /// <summary>
        /// Получение списка событий устройств из БД.
        /// </summary>
        /// <returns>Список событий устройств.</returns>
        public async Task<List<DeviceEvent>> Get()
        {
            IEnumerable<DeviceEvent> events = await connection.QueryAsync<DeviceEvent>(
                @"
                SELECT
                id, name, date, device_id, level
                FROM device_event
                ",
                transaction);

            return events.ToList();
        }

        /// <summary>
        /// Получение события устройства из БД по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор события устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task<DeviceEvent> Get(Guid id)
        {
            return await connection.QueryFirstOrDefaultAsync<DeviceEvent>(
                @"
                    SELECT
                    id, name, date, device_id, level
                    FROM device_event
                    WHERE id = @Id
                    ",
                new { Id = id },
                transaction);
        }

        /// <summary>
        /// олучение списка событий устройства из БД по идентификатору устройства.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Список событий устройства.</returns>
        public async Task<List<DeviceEvent>> GetForDevice(Guid deviceId)
        {
            IEnumerable<DeviceEvent> events = await connection.QueryAsync<DeviceEvent>(
                @"
                SELECT
                id, name, date, device_id, level
                FROM device_event
                WHERE device_id = @DeviceId
                ",
                new { DeviceId = deviceId },
                transaction);

            return events.ToList();
        }

        /// <summary>
        /// Обносление события устройства в БД.
        /// </summary>
        /// <param name="deviceEvent">Событие устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task Update(DeviceEvent deviceEvent)
        {
            await connection.ExecuteAsync(
                @"
                UPDATE device_event SET 
                name = @Name, 
                date = @Date,
                device_id = @DeviceId,
                level = @Level
                WHERE id = @Id
                ",
                deviceEvent,
                transaction);
        }
    }
}
