﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Monitoring.Mvc.Models;

namespace Monitoring.Mvc.Repositories
{
    /// <summary>
    /// Интерфейс репозитория событий устройств.
    /// </summary>
    public interface IDeviceEventRepository
    {
        /// <summary>
        /// Добавление события устройства в репозиторий.
        /// </summary>
        /// <param name="deviceEvent">Событие устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        Task Add(DeviceEvent deviceEvent);

        /// <summary>
        /// Получение количества событий устройства в репозитории по идентификатору устройства.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        Task<int> CountForDevice(Guid deviceId);

        /// <summary>
        /// Удаление события устройства из репозитория по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор события устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        Task Delete(Guid id);

        /// <summary>
        /// Удаление всех событий устройств из репозитория.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        Task DeleteAll();

        /// <summary>
        /// Удаление списка событий устройства из репозитория по идентификатору устройства.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        Task DeleteForDevice(Guid deviceId);

        /// <summary>
        /// Получение списка событий устройств из репозитория.
        /// </summary>
        /// <returns>Список событий устройств.</returns>
        Task<List<DeviceEvent>> Get();

        /// <summary>
        /// Получение события устройства из репозитория по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор события устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        Task<DeviceEvent> Get(Guid id);

        /// <summary>
        /// Получение списка событий устройства из репозитория по идентификатору устройства.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Список событий устройства.</returns>
        Task<List<DeviceEvent>> GetForDevice(Guid deviceId);

        /// <summary>
        /// Обносление события устройства в репозитории.
        /// </summary>
        /// <param name="deviceEvent">Событие устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        Task Update(DeviceEvent deviceEvent);
    }
}
