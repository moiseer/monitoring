﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Monitoring.Mvc.Models;
using NHibernate;
using NHibernate.Linq;

namespace Monitoring.Mvc.Repositories
{
    /// <summary>
    /// Репозиторий данных об устройствах.
    /// </summary>
    public class NHibernateDeviceRepository : IDeviceRepository
    {
        private readonly ISession session;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="session">Соединение с БД.</param>
        public NHibernateDeviceRepository(ISession session)
        {
            this.session = session;
        }

        /// <summary>
        /// Добавление данных об устройстве в БД.
        /// </summary>
        /// <param name="device">Данные об устройстве.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task Add(Device device)
        {
            await session.SaveAsync(device);
        }

        /// <summary>
        /// Удаление данных об устройстве из БД.
        /// </summary>
        /// <param name="id">Идентификатор устройства.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task Delete(Guid id)
        {
            var device = await session.GetAsync<Device>(id);
            await session.DeleteAsync(device);
        }

        /// <summary>
        /// Удаление данных о всех устройствах из БД.
        /// </summary>
        /// <returns>
        /// <placeholder>A <see cref="Task"/> representing the asynchronous operation.</placeholder>
        /// </returns>
        public async Task DeleteAll()
        {
            await session.Query<Device>().DeleteAsync(default);
        }

        /// <summary>
        /// Получение списка данных о всех устройствах из БД.
        /// </summary>
        /// <returns>Список данных о всех устройствах.</returns>
        public async Task<List<Device>> Get()
        {
            return await session.Query<Device>().ToListAsync();
        }

        /// <summary>
        /// Получение данных об устройстве из БД по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор устройства.</param>
        /// <returns>Данные об устройстве.</returns>
        public async Task<Device> Get(Guid id)
        {
            return await session.GetAsync<Device>(id);
        }

        /// <summary>
        /// Проверка уникальности имени узла устройства в БД.
        /// </summary>
        /// <param name="name">Имя узла.</param>
        /// <returns>Уникальность имени узла.</returns>
        public async Task<bool> IsNameUnique(string name)
        {
            return await session.Query<Device>().AnyAsync(device => device.Name == name);
        }

        /// <summary>
        /// Обновление данных об устройстве в БД.
        /// </summary>
        /// <param name="device">Данные об устройстве.</param>
        /// <returns>Результат выполнения задачи.</returns>
        public async Task Update(Device device)
        {
            await session.UpdateAsync(device);
        }
    }
}
