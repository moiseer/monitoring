﻿using System;
using Npgsql;

namespace Monitoring.Mvc.Migrations
{
    /// <summary>
    /// Инициализатор БД.
    /// </summary>
    public class DbInitializer
    {
        /// <summary>
        /// Создание БД из строки подключения, если она еще не создана.
        /// </summary>
        /// <param name="connectionString">Строка подключения к БД.</param>
        public static void CreateDbIfNotExist(string connectionString)
        {
            var stringBuilder = new NpgsqlConnectionStringBuilder(connectionString);
            string dbName = stringBuilder.Database;
            string dbUser = stringBuilder.Username;

            stringBuilder.Database = string.Empty;
            string defaultConnectionString = stringBuilder.ConnectionString;

            if (!DbExists(defaultConnectionString, dbName))
            {
                CreateDb(defaultConnectionString, dbName, dbUser);
            }
        }

        private static void CreateDb(string connectionString, string dbName, string dbUser)
        {
            string sqlCommand = $@"
                CREATE DATABASE ""{dbName}""
                    WITH 
                    OWNER = ""{dbUser}""
                    ENCODING = 'UTF8'
                    CONNECTION LIMIT = -1;
                ";

            using (var connection = new NpgsqlConnection(connectionString))
            using (var command = new NpgsqlCommand(sqlCommand, connection))
            {
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        private static bool DbExists(string connectionString, string dbName)
        {
            string sqlCommand = $@"
                SELECT 1 
                FROM pg_catalog.pg_database 
                WHERE DATNAME = '{dbName}'
                ";

            using (var connection = new NpgsqlConnection(connectionString))
            using (var command = new NpgsqlCommand(sqlCommand, connection))
            {
                connection.Open();
                var result = command.ExecuteScalar();
                connection.Close();

                return result != null;
            }
        }
    }
}
