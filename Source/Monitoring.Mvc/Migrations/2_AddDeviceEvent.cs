﻿using System;
using FluentMigrator;

namespace Monitoring.Mvc.Migrations
{
    /// <summary>
    /// Миграция БД, создающая таблицу с событиями устройств.
    /// </summary>
    [Migration(2)]
    public class AddDeviceEvent : ForwardOnlyMigration
    {
        /// <summary>
        /// Запуск миграции.
        /// </summary>
        public override void Up()
        {
            Create.Table("device_event")
                .WithColumn("id").AsGuid().PrimaryKey()
                .WithColumn("name").AsString(50).NotNullable()
                .WithColumn("date").AsDateTime().NotNullable()
                .WithColumn("device_id").AsGuid().NotNullable();

            Create.ForeignKey("fk_device_event_id")
                .FromTable("device_event").ForeignColumn("device_id")
                .ToTable("device").PrimaryColumn("id");
        }
    }
}
