﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Monitoring.Mvc.Models;
using Monitoring.Mvc.Uow;

namespace Monitoring.Mvc.Migrations
{
    /// <summary>
    /// Инициализатор тестовых данных в БД.
    /// </summary>
    public class DataInitializer
    {
        /// <summary>
        /// Заполнение БД тестовыми данными, если она пуста.
        /// </summary>
        /// <param name="unitOfWorkFactory">Фабрика логических транзакций.</param>
        /// <returns>Результат выполния задачи.</returns>
        public static async Task InitializeDataOnEmpty(IUnitOfWorkFactory unitOfWorkFactory)
        {
            using (var unitOfWork = unitOfWorkFactory.Create())
            {
                var deviceRepository = unitOfWork.GetDeviceRepository();

                List<Device> existDevices = await deviceRepository.Get();
                if (existDevices.Any())
                {
                    return;
                }

                List<Device> devices = DbTestGenerator.GetDevices();
                foreach (Device device in devices)
                {
                    await deviceRepository.Add(device);
                }

                var deviceEventRepository = unitOfWork.GetDeviceEventRepository();
                List<DeviceEvent> deviceEvents = DbTestGenerator.GetDeviceEvents();
                foreach (DeviceEvent deviceEvent in deviceEvents)
                {
                    await deviceEventRepository.Add(deviceEvent);
                }

                unitOfWork.Commit();
            }
        }
    }
}
