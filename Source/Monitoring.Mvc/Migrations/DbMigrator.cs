﻿using System;
using FluentMigrator.Runner;
using Microsoft.Extensions.DependencyInjection;

namespace Monitoring.Mvc.Migrations
{
    /// <summary>
    /// Мигратор БД.
    /// </summary>
    public class DbMigrator
    {
        /// <summary>
        /// Запуск миграций.
        /// </summary>
        /// <param name="connectionString">Строка подключения к БД.</param>
        public static void StartMigration(string connectionString)
        {
            DbInitializer.CreateDbIfNotExist(connectionString);

            IServiceProvider serviceProvider = CreateServices(connectionString);
            using (IServiceScope scope = serviceProvider.CreateScope())
            {
                UpdateDatabase(scope.ServiceProvider);
            }
        }

        private static IServiceProvider CreateServices(string connectionString)
        {
            return new ServiceCollection()
                .AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    .AddPostgres()
                    .WithGlobalConnectionString(connectionString)
                    .ScanIn(typeof(InitialMigration).Assembly).For.Migrations())
                .AddLogging(lb => lb.AddFluentMigratorConsole())
                .BuildServiceProvider(false);
        }

        private static void UpdateDatabase(IServiceProvider serviceProvider)
        {
            var runner = serviceProvider.GetRequiredService<IMigrationRunner>();

            runner.MigrateUp();
        }
    }
}
