﻿using System;
using FluentMigrator;

namespace Monitoring.Mvc.Migrations
{
    /// <summary>
    /// Миграция БД, добавляющая в таблицу "device" поле с флагом, указывающий было ли имя узла изменено через пользовательский
    /// интерфейс.
    /// </summary>
    [Migration(4)]
    public class AddIsUserDefinedNameField : ForwardOnlyMigration
    {
        /// <summary>
        /// Запуск миграции.
        /// </summary>
        public override void Up()
        {
            Alter.Table("device")
                .AddColumn("is_user_defined_name")
                .AsBoolean()
                .SetExistingRowsTo(false)
                .NotNullable();
        }
    }
}
