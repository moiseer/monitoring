﻿using System;
using FluentMigrator;

namespace Monitoring.Mvc.Migrations
{
    /// <summary>
    /// Миграция БД, создающая таблицу с данными об устройстве.
    /// </summary>
    [Migration(1)]
    public class InitialMigration : ForwardOnlyMigration
    {
        /// <summary>
        /// Запуск миграции.
        /// </summary>
        public override void Up()
        {
            Create.Table("device")
                .WithColumn("id").AsGuid().PrimaryKey()
                .WithColumn("name").AsString(50).NotNullable()
                .WithColumn("last_statistic_date").AsDateTime().NotNullable()
                .WithColumn("date_received").AsDateTime().NotNullable()
                .WithColumn("operation_system").AsString(50).NotNullable()
                .WithColumn("version").AsString(50).NotNullable();
        }
    }
}
