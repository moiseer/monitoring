﻿using System;
using FluentMigrator;

namespace Monitoring.Mvc.Migrations
{
    /// <summary>
    /// Миграция БД, добавляющая уникальный индекс на поле "name" в таблице "device".
    /// </summary>
    [Migration(3)]
    public class AddDeviceNameUnique : ForwardOnlyMigration
    {
        /// <summary>
        /// Запуск миграции.
        /// </summary>
        public override void Up()
        {
            Alter.Column("name").OnTable("device").AsString(50).NotNullable().Unique();
        }
    }
}
