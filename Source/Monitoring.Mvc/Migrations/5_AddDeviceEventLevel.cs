﻿using System;
using FluentMigrator;

namespace Monitoring.Mvc.Migrations
{
    /// <summary>
    /// Миграция БД, добавляюцая в таблицу "device_event" поле с уровнем критичности события.
    /// </summary>
    [Migration(5)]
    public class AddDeviceEventLevel : ForwardOnlyMigration
    {
        /// <summary>
        /// Запуск миграции.
        /// </summary>
        public override void Up()
        {
            Alter.Table("device_event")
                .AddColumn("level")
                .AsInt32()
                .SetExistingRowsTo(0)
                .NotNullable();
        }
    }
}
