﻿using System;
using System.Collections.Generic;
using System.Linq;
using Monitoring.Contracts;
using Monitoring.Mvc.Models;

namespace Monitoring.Mvc.Migrations
{
    /// <summary>
    /// Генератор тестовых значений бля БД.
    /// </summary>
    public static class DbTestGenerator
    {
        private static List<DeviceEvent> deviceEvents;
        private static List<Device> devices;

        /// <summary>
        /// Конструктор.
        /// </summary>
        static DbTestGenerator()
        {
            Generate();
        }

        /// <summary>
        /// Получить сгенерированные события устройств.
        /// </summary>
        /// <returns>Список событий устройствах.</returns>
        public static List<DeviceEvent> GetDeviceEvents()
        {
            return deviceEvents;
        }

        /// <summary>
        /// Получить сгенерированные данные устройств.
        /// </summary>
        /// <returns>Список данных об устройствах.</returns>
        public static List<Device> GetDevices()
        {
            return devices;
        }

        private static void Generate()
        {
            GenerateDevices();

            Guid[] deviceIds = devices.Select(d => d.Id).ToArray();
            GenerateDeviceEvents(deviceIds);
        }

        private static void GenerateDeviceEvents(Guid[] deviceIds)
        {
            deviceEvents = new List<DeviceEvent>();
            var random = new Random();
            foreach (Guid deviceId in deviceIds)
            {
                for (int i = 0; i < 30; i++)
                {
                    deviceEvents.Add(new DeviceEvent
                    {
                        DeviceId = deviceId,
                        Id = Guid.NewGuid(),
                        Date = DateTime.Now,
                        Level = (DeviceEventLevel)random.Next(0, 3),
                        Name = $"Test Event {i}"
                    });
                }
            }
        }

        private static void GenerateDevices()
        {
            devices = new List<Device>();
            for (int i = 0; i < 30; i++)
            {
                devices.Add(new Device
                {
                    Id = Guid.NewGuid(),
                    Name = $"Test Node {i}",
                    LastStatisticDate = DateTime.Now,
                    DateReceived = DateTime.Now,
                    OperationSystem = "Win 10",
                    Version = "1.0.0.0",
                    IsUserDefinedName = false
                });
            }
        }
    }
}
