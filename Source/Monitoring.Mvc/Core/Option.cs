using System;

namespace Monitoring.Mvc.Core
{
    /// <summary>
    /// Реализация мондаты MayBe.
    /// </summary>
    /// <typeparam name="T">Тип значения.</typeparam>
    public struct Option<T>
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="value">Значение.</param>
        /// <param name="hasValue">Флаг, указывающий, что значение не пусто.</param>
        public Option(T value, bool hasValue = true)
        {
            HasValue = hasValue;
            Value = value;
        }

        /// <summary>
        /// Объект с пустым значением заданного типа.
        /// </summary>
        public static Option<T> Empty { get; } = new Option<T>(default(T), false);

        /// <summary>
        /// Флаг, указывающий, что значение пусто.
        /// </summary>
        public bool HasNoValue => !HasValue;

        /// <summary>
        /// Флаг, указывающий, что значение не пусто.
        /// </summary>
        public bool HasValue { get; }

        /// <summary>
        /// Значение.
        /// </summary>
        public T Value { get; }

        /// <summary>
        /// Выполнение действия над значением при выполнении условия.
        /// </summary>
        /// <param name="predicate">Условие.</param>
        /// <param name="action">Действие.</param>
        /// <returns>Текущий объект Option.</returns>
        public Option<T> Match(Predicate<T> predicate, Action<T> action)
        {
            if (HasNoValue)
            {
                return Empty;
            }

            if (predicate(Value))
            {
                action(Value);
            }

            return this;
        }

        /// <summary>
        /// Выполнение действия над значением, если оно заданного типа.
        /// </summary>
        /// <param name="action">Действие.</param>
        /// <typeparam name="TTarget">Заданный тип значения.</typeparam>
        /// <returns>Текущий объект Option.</returns>
        public Option<T> MatchType<TTarget>(Action<TTarget> action)
            where TTarget : T
        {
            if (HasNoValue)
            {
                return Empty;
            }

            if (Value.GetType() == typeof(TTarget))
            {
                action((TTarget)Value);
            }

            return this;
        }
    }
}
