﻿using System;
using System.Net;

namespace Monitoring.Mvc.Core
{
    /// <summary>
    /// Ошибка, связанная с сервисом мониторинга.
    /// </summary>
    public class MonitoringException : Exception
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="statusCode">Статус код ошибки.</param>
        public MonitoringException(HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            StatusCode = statusCode;
        }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="message">Сообщение ошибки.</param>
        /// <param name="statusCode">Статус код ошибки.</param>
        public MonitoringException(string message, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
            : base(message)
        {
            StatusCode = statusCode;
        }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="message">Сообщение ошибки.</param>
        /// <param name="innerException">Внутреннее исключение.</param>
        /// <param name="statusCode">Статус код ошибки.</param>
        public MonitoringException(string message, Exception innerException, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
            : base(message, innerException)
        {
            StatusCode = statusCode;
        }

        /// <summary>
        /// Статус код ошибки.
        /// </summary>
        public HttpStatusCode StatusCode { get; }
    }
}
