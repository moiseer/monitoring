﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Serilog;

namespace Monitoring.Mvc.Core
{
    /// <summary>
    /// Обработчик для ошибок сервиса мониторинга.
    /// </summary>
    public class MonitoringExceptionAttribute : ExceptionFilterAttribute
    {
        private readonly ILogger logger = Log.Logger.ForContext<MonitoringExceptionAttribute>();

        /// <summary>
        /// Обработка ошибки.
        /// </summary>
        /// <param name="context">Контекст ошибки.</param>
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is MonitoringException monitoringException)
            {
                var result = new ObjectResult(monitoringException.Message);
                logger.Warning("Перехвачена ошибка: {ErrorMessage}", monitoringException.Message);
                result.StatusCode = (int)monitoringException.StatusCode;
                context.Result = result;
            }
            else
            {
                var result = new ObjectResult("Unexpected error");
                logger.Error("Unexpected error. {@ErrorMessage}", context.Exception);
                result.StatusCode = 500;
                context.Result = result;
            }

            context.ExceptionHandled = true;
        }
    }
}
