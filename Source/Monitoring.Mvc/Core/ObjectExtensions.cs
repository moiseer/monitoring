﻿using System;

namespace Monitoring.Mvc.Core
{
    /// <summary>
    /// Методы расширения класса Object.
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// Проверка, является ли значение пустым.
        /// </summary>
        /// <param name="value">Проверяемое значение.</param>
        /// <typeparam name="T">Тип значения.</typeparam>
        /// <returns>Является ли значение пустым.</returns>
        public static bool IsEmpty<T>(this T value)
        {
            if (!typeof(T).IsValueType && value == null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Проверка, не является ли значение пустым.
        /// </summary>
        /// <param name="value">Проверяемое значение.</param>
        /// <typeparam name="T">Тип значения.</typeparam>
        /// <returns>Не является ли значение пустым.</returns>
        public static bool IsNotEmpty<T>(this T value)
        {
            return !value.IsEmpty();
        }

        /// <summary>
        /// Преобразование значения в объект класса Option.
        /// </summary>
        /// <param name="value">Значение.</param>
        /// <typeparam name="T">Тип значения.</typeparam>
        /// <returns>Преобразованное в Option значение.</returns>
        public static Option<T> ToOption<T>(this T value)
        {
            if (value.IsEmpty())
            {
                return Option<T>.Empty;
            }

            return new Option<T>(value);
        }
    }
}
