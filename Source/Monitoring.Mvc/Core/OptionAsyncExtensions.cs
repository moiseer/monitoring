﻿using System;
using System.Threading.Tasks;

namespace Monitoring.Mvc.Core
{
    /// <summary>
    /// Асинхронные методы расширения класса Option.
    /// </summary>
    public static class OptionAsyncExtensions
    {
        /// <summary>
        /// Асинхронное выполнение асинхронной функции.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="action">Асинхронная функция.</param>
        /// <typeparam name="T">Тип значения объекта.</typeparam>
        /// <returns>Объект Option.</returns>
        public static async Task<T> DoAsync<T>(this Option<T> value, Func<T, Task> action)
        {
            if (value.HasValue)
            {
                await action(value.Value);
            }

            return value.Value;
        }

        /// <summary>
        /// Асинхронное выполнение асинхронной функции, если значение объекта пусто.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="action">Асинхронная функция.</param>
        /// <typeparam name="T">Тип значения объекта.</typeparam>
        /// <returns>Объект Option.</returns>
        public static async Task DoOnEmptyAsync<T>(this Option<T> value, Func<Task> action)
        {
            if (value.HasNoValue)
            {
                await action();
            }
        }

        /// <summary>
        /// Синхронное выполнение асинхронной функции, если значение объекта пусто.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="action">Асинхронная функция.</param>
        /// <typeparam name="T">Тип значения объекта.</typeparam>
        /// <returns>Объект Option.</returns>
        public static Option<T> DoOnEmptySync<T>(this Option<T> value, Func<Task> action)
        {
            if (value.HasNoValue)
            {
                action().Wait();
            }

            return value;
        }

        /// <summary>
        /// Синхронное выполнение асинхронной функции.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="action">Асинхронная функция.</param>
        /// <typeparam name="T">Тип значения объекта.</typeparam>
        /// <returns>Объект Option.</returns>
        public static Option<T> DoSync<T>(this Option<T> value, Func<T, Task> action)
        {
            if (value.HasValue)
            {
                action(value.Value).Wait();
            }

            return value;
        }

        /// <summary>
        /// Асинхронное преобразование объекта с помощью асинхронной функции.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="func">Асинхронная функция преобразования.</param>
        /// <typeparam name="TInput">Исходный тип значения объекта.</typeparam>
        /// <typeparam name="TResult">Целевой тип значения объекта.</typeparam>
        /// <returns>Результат преобразования или пустое значение.</returns>
        public static async Task<TResult> MapAsync<TInput, TResult>(this Option<TInput> value, Func<TInput, Task<TResult>> func)
        {
            if (value.HasValue)
            {
                return await func(value.Value);
            }

            return default;
        }

        /// <summary>
        /// Асинхронное преобразование объекта с помощью асинхронной функции, если значение объекта пусто.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="func">Асинхронная функция преобразования.</param>
        /// <typeparam name="T">Целевой тип значения объекта.</typeparam>
        /// <returns>Результат преобразования или исходный объект.</returns>
        public static async Task<T> MapOnEmptyAsync<T>(this Option<T> value, Func<Task<T>> func)
        {
            if (value.HasNoValue)
            {
                return await func();
            }

            return value.Value;
        }

        /// <summary>
        /// Синхронное преобразование объекта с помощью асинхронной функции, если значение объекта пусто.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="func">Асинхронная функция преобразования.</param>
        /// <typeparam name="T">Целевой тип значения объекта.</typeparam>
        /// <returns>Результат преобразования или исходный объект.</returns>
        public static Option<T> MapOnEmptySync<T>(this Option<T> value, Func<Task<T>> func)
        {
            if (value.HasNoValue)
            {
                return func().Result.ToOption();
            }

            return value;
        }

        /// <summary>
        /// Синхронное преобразование объекта с помощью асинхронной функции.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="func">Асинхронная функция преобразования.</param>
        /// <typeparam name="TInput">Исходный тип значения объекта.</typeparam>
        /// <typeparam name="TResult">Целевой тип значения объекта.</typeparam>
        /// <returns>Результат преобразования или пустой объект.</returns>
        public static Option<TResult> MapSync<TInput, TResult>(this Option<TInput> value, Func<TInput, Task<TResult>> func)
        {
            if (value.HasValue)
            {
                return func(value.Value).Result.ToOption();
            }

            return Option<TResult>.Empty;
        }

        /// <summary>
        /// Асинхронный вызов исключения заданного типа из асинхронной функции, если значение объекта пусто.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="func">Асинхронная функция с возвратом исключения.</param>
        /// <typeparam name="T">Тип значения объекта.</typeparam>
        /// <typeparam name="TException">Тип исключения.</typeparam>
        /// <returns>Исходный объект Option.</returns>
        public static async Task<T> ThrowOnEmptyAsync<T, TException>(this Option<T> value, Func<Task<TException>> func)
            where TException : Exception
        {
            if (value.HasNoValue)
            {
                throw await func();
            }

            return value.Value;
        }

        /// <summary>
        /// Синхронный вызов исключения заданного типа из асинхронной функции, если значение объекта пусто.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="func">Асинхронная функция с возвратом исключения.</param>
        /// <typeparam name="T">Тип значения объекта.</typeparam>
        /// <typeparam name="TException">Тип исключения.</typeparam>
        /// <returns>Исходный объект Option.</returns>
        public static Option<T> ThrowOnEmptySync<T, TException>(this Option<T> value, Func<Task<TException>> func)
            where TException : Exception
        {
            if (value.HasNoValue)
            {
                throw func().Result;
            }

            return value;
        }
    }
}
