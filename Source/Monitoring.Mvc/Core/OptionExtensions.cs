﻿using System;

namespace Monitoring.Mvc.Core
{
    /// <summary>
    /// Методы расширения класса Option.
    /// </summary>
    public static class OptionExtensions
    {
        /// <summary>
        /// Выполнение действия над значением объекта.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="action">Действие.</param>
        /// <typeparam name="T">Тип значения объекта.</typeparam>
        /// <returns>Объект Option.</returns>
        public static Option<T> Do<T>(this Option<T> value, Action<T> action)
        {
            if (value.HasValue)
            {
                action(value.Value);
            }

            return value;
        }

        /// <summary>
        /// Выполнение действия над значением объекта при выполнении условия.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="predicate">Условие.</param>
        /// <param name="action">Действие.</param>
        /// <typeparam name="T">Тип значения объекта.</typeparam>
        /// <returns>Объект Option.</returns>
        public static Option<T> Do<T>(this Option<T> value, Predicate<T> predicate, Action<T> action)
        {
            if (value.HasNoValue)
            {
                return value;
            }

            if (predicate(value.Value))
            {
                action(value.Value);
            }

            return value;
        }

        /// <summary>
        /// Выполнение действия, если значение объекта пусто.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="action">Действие.</param>
        /// <typeparam name="T">Тип значения объекта.</typeparam>
        /// <returns>Объект Option.</returns>
        public static Option<T> DoOnEmpty<T>(this Option<T> value, Action action)
        {
            if (value.HasNoValue)
            {
                action();
            }

            return value;
        }

        /// <summary>
        /// Выполнение действия над значением объекта и возврат итогового значения.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="action">Действие.</param>
        /// <typeparam name="T">Тип значения объекта.</typeparam>
        /// <returns>Значение объекта.</returns>
        public static T Finally<T>(this Option<T> value, Action<T> action)
        {
            action(value.Value);
            return value.Value;
        }

        /// <summary>
        /// Преобразование объекта с помощью функции.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="func">Функция преобразования.</param>
        /// <typeparam name="TInput">Исходный тип значения объекта.</typeparam>
        /// <typeparam name="TResult">Целевой тип значения объекта.</typeparam>
        /// <returns>Результат преобразования.</returns>
        public static Option<TResult> Map<TInput, TResult>(this Option<TInput> value, Func<TInput, Option<TResult>> func)
        {
            if (value.HasNoValue)
            {
                return Option<TResult>.Empty;
            }

            return func(value.Value);
        }

        /// <summary>
        /// Преобразование объекта с помощью функции.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="func">Функция преобразования.</param>
        /// <typeparam name="TInput">Исходный тип значения объекта.</typeparam>
        /// <typeparam name="TResult">Целевой тип значения объекта.</typeparam>
        /// <returns>Результат преобразования.</returns>
        public static Option<TResult> Map<TInput, TResult>(this Option<TInput> value, Func<TInput, TResult> func)
        {
            if (value.HasNoValue)
            {
                return Option<TResult>.Empty;
            }

            return func(value.Value).ToOption();
        }

        /// <summary>
        /// Преобразование объекта с помощью функции при выполнении условия.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="predicate">Условие.</param>
        /// <param name="func">Функция преобразования.</param>
        /// <typeparam name="TInput">Исходный тип значения объекта.</typeparam>
        /// <typeparam name="TResult">Целевой тип значения объекта.</typeparam>
        /// <returns>Результат преобразования или пустой объект.</returns>
        public static Option<TResult> Map<TInput, TResult>(this Option<TInput> value, Predicate<TInput> predicate, Func<TInput, TResult> func)
        {
            if (value.HasNoValue)
            {
                return Option<TResult>.Empty;
            }

            if (!predicate(value.Value))
            {
                return Option<TResult>.Empty;
            }

            return func(value.Value).ToOption();
        }

        /// <summary>
        /// Синхронное преобразование объекта с помощью асинхронной функции, если значение объекта пусто.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="func">Асинхронная функция преобразования.</param>
        /// <typeparam name="T">Целевой тип значения объекта.</typeparam>
        /// <returns>Результат преобразования или исходный объект.</returns>
        public static Option<T> MapOnEmpty<T>(this Option<T> value, Func<T> func)
        {
            if (value.HasNoValue)
            {
                return func().ToOption();
            }

            return value;
        }

        /// <summary>
        /// Вызов исключения заданного типа, если значение объекта пусто.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <typeparam name="T">Тип значения объекта.</typeparam>
        /// <typeparam name="TException">Тип исключения.</typeparam>
        /// <returns>Исходный объект Option.</returns>
        public static Option<T> ThrowOnEmpty<T, TException>(this Option<T> value)
            where TException : Exception, new()
        {
            if (value.HasValue)
            {
                return value;
            }

            throw new TException();
        }

        /// <summary>
        /// Вызов исключения заданного типа из функции, если значение объекта пусто.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="func">Функция с возвратом исключения.</param>
        /// <typeparam name="T">Тип значения объекта.</typeparam>
        /// <typeparam name="TException">Тип исключения.</typeparam>
        /// <returns>Исходный объект Option.</returns>
        public static Option<T> ThrowOnEmpty<T, TException>(this Option<T> value, Func<TException> func)
            where TException : Exception
        {
            if (value.HasValue)
            {
                return value;
            }

            throw func();
        }

        /// <summary>
        /// Возврат объекта при выполнении условия.
        /// </summary>
        /// <param name="value">Объект Option.</param>
        /// <param name="predicate">Условие.</param>
        /// <typeparam name="T">Тип значения объекта.</typeparam>
        /// <returns>Исходный или пустой объект.</returns>
        public static Option<T> Where<T>(this Option<T> value, Predicate<T> predicate)
        {
            if (value.HasNoValue)
            {
                return Option<T>.Empty;
            }

            return predicate(value.Value) ? value : Option<T>.Empty;
        }
    }
}
