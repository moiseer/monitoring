﻿using System;
using System.Net;
using System.Threading.Tasks;
using Monitoring.Contracts;
using Monitoring.Mvc.Core;
using Monitoring.Mvc.Mediator.Requests;
using Monitoring.Mvc.Repositories;
using Serilog;

namespace Monitoring.Mvc.Validators
{
    /// <summary>
    /// Валидатор данных об устройстве.
    /// </summary>
    public class DeviceValidator : IDeviceValidator
    {
        private readonly ILogger logger = Log.ForContext<DeviceValidator>();

        /// <summary>
        /// Проверка валидности данных об устройстве.
        /// </summary>
        /// <param name="device">Данные об устройстве.</param>
        public void Validate(DeviceRequest device)
        {
            ValidateNameLength(device.Id, device.Name);

            device.Events.ToOption()
                .DoOnEmpty(() => logger.Warning("Список событий устройства {@device} не заполнен.", device))
                .ThrowOnEmpty(() => new MonitoringException("Список событий не заполнен."));

            foreach (DeviceEventRequest deviceEvent in device.Events)
            {
                Validate(deviceEvent);
            }
        }

        /// <summary>
        /// Проверка валидности события устройства.
        /// </summary>
        /// <param name="deviceEvent">Событие устройства.</param>
        public void Validate(DeviceEventRequest deviceEvent)
        {
            deviceEvent?.Name.ToOption()
                .DoOnEmpty(() => logger.Warning("Имя события {@deviceEvent} не заполнено.", deviceEvent))
                .ThrowOnEmpty(() => new MonitoringException("Имя события не заполнено."));

            deviceEvent.Name.Length.ToOption()
                .Where(length => length > 0 && length <= 50)
                .DoOnEmpty(() => logger.Warning("Имя события {@deviceEvent} не прошло валидацию.", deviceEvent))
                .ThrowOnEmpty(() => new MonitoringException("Имя события не прошло валидацию."));
        }

        /// <summary>
        /// Проверка длины имени узла устройства.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <param name="name">Имя узла устройства.</param>
        public void ValidateNameLength(Guid deviceId, string name)
        {
            name.ToOption()
                .DoOnEmpty(() => logger.Warning("Имя устройства {Id} не заполнено.", deviceId))
                .ThrowOnEmpty(() => new MonitoringException($"Имя устройства {deviceId} не заполнено."));

            name.Length.ToOption()
                .Where(length => length > 0 && length <= 50)
                .DoOnEmpty(() => logger.Warning("Имя {Name} устройства {Id} не прошло валидацию.",
                    name,
                    deviceId))
                .ThrowOnEmpty(() => new MonitoringException($"Имя {name} устройства {deviceId} не прошло валидацию."));
        }

        /// <summary>
        /// Проверка уникальности имени узла устройства.
        /// </summary>
        /// <param name="name">Имя узла устройства.</param>
        /// <param name="repository">Репозиторий данных об устройствах.</param>
        /// <returns>Результат выполниеня задачи.</returns>
        public async Task ValidateNameUnique(string name, IDeviceRepository repository)
        {
            if (!await repository.IsNameUnique(name))
            {
                logger.Warning("Имя {Name} устройства не уникально.", name);
                throw new MonitoringException($"Имя {name} устройства не уникально.", HttpStatusCode.Conflict);
            }
        }

        /// <summary>
        /// Проверка уникальности имени узла устройства.
        /// </summary>
        /// <param name="request">Запрос на изменение имени узла устройства.</param>
        /// <returns>Результат выполниеня задачи.</returns>
        public async Task ValidateNameUnique(UpdateDeviceNameMediatorRequest request)
        {
            request.DeviceRepository.ToOption()
                .DoOnEmpty(() => logger.Error("Отсутствует ссылка на репозиторий."))
                .ThrowOnEmpty(() => new ApplicationException("Отсутствует ссылка на репозиторий."));

            if (!await request.DeviceRepository.IsNameUnique(request.Name))
            {
                logger.Warning("Имя {Name} устройства {Id} не уникально.", request.Name, request.DeviceId);
                throw new MonitoringException($"Имя {request.Name} устройства {request.DeviceId} не уникально.", HttpStatusCode.Conflict);
            }
        }
    }
}
