﻿using System;
using System.Threading.Tasks;
using Monitoring.Contracts;
using Monitoring.Mvc.Mediator.Requests;
using Monitoring.Mvc.Repositories;

namespace Monitoring.Mvc.Validators
{
    /// <summary>
    /// Интерфейс валидатора данных об устройстве.
    /// </summary>
    public interface IDeviceValidator
    {
        /// <summary>
        /// Проверка валидности данных об устройстве.
        /// </summary>
        /// <param name="device">Данные об устройстве.</param>
        void Validate(DeviceRequest device);

        /// <summary>
        /// Проверка валидности события устройства.
        /// </summary>
        /// <param name="deviceEvent">Событие устройства.</param>
        void Validate(DeviceEventRequest deviceEvent);

        /// <summary>
        /// Проверка длины имени узла устройства.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <param name="name">Имя узла устройства.</param>
        void ValidateNameLength(Guid deviceId, string name);

        /// <summary>
        /// Проверка уникальности имени узла устройства.
        /// </summary>
        /// <param name="name">Имя узла устройства.</param>
        /// <param name="repository">Репозиторий данных об устройствах.</param>
        /// <returns>Результат выполниеня задачи.</returns>
        Task ValidateNameUnique(string name, IDeviceRepository repository);

        /// <summary>
        /// Проверка уникальности имени узла устройства.
        /// </summary>
        /// <param name="request">Запрос на изменение имени узла устройства.</param>
        /// <returns>Результат выполниеня задачи.</returns>
        Task ValidateNameUnique(UpdateDeviceNameMediatorRequest request);
    }
}
