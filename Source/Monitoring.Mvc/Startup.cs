using System;
using System.IO;
using System.Reflection;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Monitoring.Contracts;
using Monitoring.Mvc.Core;
using Monitoring.Mvc.Mediator.Behaviors;
using Monitoring.Mvc.Mediator.Requests;
using Monitoring.Mvc.MessageSender;
using Monitoring.Mvc.Migrations;
using Monitoring.Mvc.Models;
using Monitoring.Mvc.Services;
using Monitoring.Mvc.SignalR.Hubs;
using Monitoring.Mvc.Uow;
using Monitoring.Mvc.Validators;
using Serilog;

namespace Monitoring.Mvc
{
    /// <summary>
    /// Входная точка в приложение ASP.NET Core.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="configuration">Конфигурация приложения.</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .WriteTo.ColoredConsole(outputTemplate: "[{Timestamp:HH:mm:ss}] [{Level:u3}] [{SourceContext}] {Message:lj}{NewLine}{Exception}")
                .WriteTo.Seq("http://localhost:5341")
                .CreateLogger();
        }

        /// <summary>
        /// Конфигурация приложения.
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Конфигурация маппинга <see cref="Mapster"/>.
        /// </summary>
        public static void ConfigureMapping()
        {
            TypeAdapterConfig<DeviceRequest, Device>.NewConfig()
                .Map(dest => dest.OperationSystem, src => src.Os)
                .Map(dest => dest.DateReceived, scr => DateTime.Now);
            TypeAdapterConfig<DeviceEventRequest, DeviceEvent>.NewConfig()
                .Map(dest => dest.Id, src => Guid.NewGuid());
        }

        /// <summary>
        /// Конфигурация обработки запросов приложением.
        /// </summary>
        /// <param name="app">Класс, который предоставляет механизмы для настройки конвейера запросов приложения.</param>
        /// <param name="env">Информация о среде, в которой запускается приложение.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder => builder
                .SetIsOriginAllowed(host => true)
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials()
            );

            app.UseHttpsRedirection();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    "default",
                    "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapHub<ClientHub>("/clientHub");
            });

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Monitoring API V1"); });

            ConfigureMapping();

            DbMigrator.StartMigration(Configuration["ConnectionString"]);

            if (Configuration.GetValue<bool>("UseTestData"))
            {
                DataInitializer.InitializeDataOnEmpty(new UnitOfWorkFactory(Configuration)).Wait();
            }
        }

        /// <summary>
        /// Регистрация сервисов, которые используются приложением.
        /// </summary>
        /// <param name="services">Коллекция сервисов приложения.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.AddMvc(option => { option.Filters.Add(typeof(MonitoringExceptionAttribute)); });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Version = "v1",
                        Title = "Monitoring API",
                        Description = "ASP.NET Core Web API"
                    });

                string xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                string xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            services.AddSingleton<IDeviceService, DeviceService>();
            services.AddSingleton<IUnitOfWorkFactory, UnitOfWorkFactory>();
            services.AddSingleton<IDeviceValidator, DeviceValidator>();
            services.AddSingleton(Configuration);

            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddSingleton<IPipelineBehavior<UpdateDeviceNameMediatorRequest, Unit>, ValidateDeviceNameBehaviour>();

            services.AddSingleton<IMessageSender, RabbitSender>();
            services.AddSingleton<IMessageService, MessageService>();

            services.AddSignalR();
            services.AddSingleton<IClientNotifyService, ClientNotifyService>();
        }
    }
}
