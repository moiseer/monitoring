CREATE TABLE device(
   Id UUID PRIMARY KEY,
   Name VARCHAR (256) NOT NULL,
   LastStatisticDate timestamp NOT NULL,
   DateReceived timestamp NOT NULL,
   OperationSystem varchar (50) NOT NULL,
   Version varchar (50) NOT NULL
);