﻿using System;
using Monitoring.Mvc.Models;
using NHibernate.Mapping.ByCode.Conformist;

namespace Monitoring.Mvc.NHibernate
{
    /// <summary>
    /// Маппинг класса Device.
    /// </summary>
    public class DeviceMap : ClassMapping<Device>
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        public DeviceMap()
        {
            Table("device");
            Id(x => x.Id, map => map.Column("id"));
            Property(x => x.Name, map => map.Column("name"));
            Property(x => x.Version, map => map.Column("version"));
            Property(x => x.DateReceived, map => map.Column("date_received"));
            Property(x => x.OperationSystem, map => map.Column("operation_system"));
            Property(x => x.LastStatisticDate, map => map.Column("last_statistic_date"));
            Property(x => x.IsUserDefinedName, map => map.Column("is_user_defined_name"));
        }
    }
}
