﻿using System;
using Monitoring.Mvc.Models;
using NHibernate.Mapping.ByCode.Conformist;

namespace Monitoring.Mvc.NHibernate
{
    /// <summary>
    /// Маппинг для класса DeviceEvent.
    /// </summary>
    public class DeviceEventMap : ClassMapping<DeviceEvent>
    {
        /// <summary>
        /// Контструктор.
        /// </summary>
        public DeviceEventMap()
        {
            Table("device_event");
            Id(x => x.Id, map => map.Column("id"));
            Property(x => x.Name, map => map.Column("name"));
            Property(x => x.Date, map => map.Column("date"));
            Property(x => x.Level, map => map.Column("level"));
            Property(x => x.DeviceId, map => map.Column("device_id"));
        }
    }
}
