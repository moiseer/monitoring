﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Monitoring.Contracts;
using Monitoring.Mvc.Services;
using Serilog;

namespace Monitoring.Mvc.Controllers
{
    /// <summary>
    /// Контроллер для работы с данными устройств для клиентов.
    /// </summary>
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class DeviceController : ControllerBase
    {
        private readonly ILogger logger = Log.ForContext<DeviceController>();
        private readonly IDeviceService service;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="service">Сервис для работы с данными устройств.</param>
        public DeviceController(IDeviceService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Проверка уникальности имени узла устройства.
        /// </summary>
        /// <param name="name">Имя узла.</param>
        /// <response code="200">Имя узла уникально.</response>
        /// <response code="409">Имя узла не уникально.</response>
        /// <returns>Уникальность имени устройства.</returns>
        [HttpGet("{name}/unique")]
        public async Task<IActionResult> CheckDeviceNameUnique(string name)
        {
            var result = await service.CheckDeviceNameUnique(name);

            return Ok(result);
        }

        /// <summary>
        /// Удаление списка событий устройства по идентификатору.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <response code="200">Список событий устройства успешно удален.</response>
        /// <response code="404">Не удалось удалить список событий устройства.</response>
        /// <returns>Результат удаления списка событий устройства.</returns>
        [HttpDelete("{deviceId:guid}/events")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult> DeleteEventsForDevice(Guid deviceId)
        {
            logger.Information("Удаление списка событий устройства по идентификатору {Id}.", deviceId);

            await service.DeleteEventsForDevice(deviceId);

            return Ok();
        }

        /// <summary>
        /// Получение списка данных о всех устройствах.
        /// </summary>
        /// <returns>Список данных о всех устройствах.</returns>
        /// <response code="200">Получен список данных о всех устройствах.</response>
        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<ActionResult<IEnumerable<DeviceResponse>>> Get()
        {
            logger.Information("Получение списка данных о всех устройствах.");

            List<DeviceResponse> devices = await service.Get();

            return Ok(devices);
        }

        /// <summary>
        /// Получение данных об устройстве по идентификатору.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Данные об устройстве.</returns>
        /// <response code="200">Данные об устройстве успешно получены.</response>
        /// <response code="404">Не удалось получить данные об устройстве.</response>
        [HttpGet("{deviceId:guid}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<DeviceResponse>> Get(Guid deviceId)
        {
            logger.Information("Получение данных об устройстве по идентификатору {Id}", deviceId);

            var device = await service.Get(deviceId);

            return Ok(device);
        }

        /// <summary>
        /// Получение списка событий устройства по идентификатору.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <returns>Список событий устройства.</returns>
        /// <response code="200">Список событий устройства успешно получен.</response>
        /// <response code="404">Не удалось получить список событий устройства.</response>
        [HttpGet("{deviceId:guid}/events")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<DeviceEventsResponse>> GetEventsForDevice(Guid deviceId)
        {
            logger.Information("Получение списка событий устройства по идентификатору {Id}.", deviceId);

            DeviceEventsResponse deviceEvents = await service.GetEventsForDevice(deviceId);

            return Ok(deviceEvents);
        }

        /// <summary>
        /// Измененние имени узла устройства.
        /// </summary>
        /// <param name="request">Запрос на изменение имени узла.</param>
        /// <response code="200">Данные об устройстве успешно обновлены.</response>
        /// <response code="404">Не удалось обновить данные об устройстве.</response>
        /// <response code="409">Имя узла не уникально.</response>
        /// <returns>Результат изменения имени узла устройства.</returns>
        [HttpPatch]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(409)]
        public async Task<IActionResult> Patch([FromBody] UpdateDeviceNameRequest request)
        {
            logger.Information("Измененние имени узла устройства {Id} на {Name}.", request.DeviceId, request.Name);

            await service.UpdateName(request);

            return Ok();
        }
    }
}
