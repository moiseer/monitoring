﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Monitoring.Contracts;
using Monitoring.Mvc.Services;
using Serilog;

namespace Monitoring.Mvc.Controllers
{
    /// <summary>
    /// Контроллер для работы с данными устройств на устройстве.
    /// </summary>
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class StatisticController : Controller
    {
        private readonly ILogger logger = Log.ForContext<StatisticController>();
        private readonly IDeviceService service;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="service">Сервис для работы с данными устройств.</param>
        public StatisticController(IDeviceService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Удаление данных об устройстве по идентификатору.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <response code="200">Данные об устройстве успешно удалены.</response>
        /// <response code="404">Не удалось удалить данные об устройстве.</response>
        /// <returns>Результат удаления данных об устройстве.</returns>
        [HttpDelete("{deviceId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Delete(Guid deviceId)
        {
            logger.Information("Удаление данных об устройстве {Id}.", deviceId);

            await service.Delete(deviceId);

            return Ok();
        }

        /// <summary>
        /// Добавление данных об устройстве.
        /// </summary>
        /// <param name="device">Данные об устройстве.</param>
        /// <response code="200">Данные об устройстве успешно добавлены.</response>
        /// <response code="400">Не удалось добавить данные об устройстве.</response>
        /// <returns>Результат добавления данных об устройстве.</returns>
        [HttpPut]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Put([FromBody] DeviceRequest device)
        {
            logger.Information("Сохранение данных об устройстве {Id}.", device.Id);

            await service.AddOrUpdate(device);

            return Ok();
        }
    }
}
