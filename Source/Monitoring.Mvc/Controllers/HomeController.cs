﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Monitoring.Mvc.Services;
using Serilog;

namespace Monitoring.Mvc.Controllers
{
    /// <summary>
    /// Контроллер для отображения данных устройств.
    /// </summary>
    public class HomeController : Controller
    {
        private readonly ILogger logger = Log.ForContext<HomeController>();
        private readonly IDeviceService service;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="service">Сервис для работы с данными устройств.</param>
        public HomeController(IDeviceService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Получение списка событий устройства по идентификатору.
        /// </summary>
        /// <param name="deviceId">Идентификатор устройства.</param>
        /// <param name="deviceName">Имя узла устройства.</param>
        /// <returns>Список событий устройства на странице.</returns>
        public async Task<IActionResult> DeviceEvents(Guid deviceId, string deviceName)
        {
            logger.Information("Запрос событий устройства {Id}.", deviceId);
            ViewData["DeviceId"] = $"\"{deviceName}\"";
            return View(await service.GetEventsForDevice(deviceId));
        }

        /// <summary>
        /// Получение списка данных о всех устройствах.
        /// </summary>
        /// <returns>Список данных о всех устройствах на странице.</returns>
        public async Task<IActionResult> Index()
        {
            logger.Information("Запрос данных из памяти.");
            return View(await service.Get());
        }
    }
}
