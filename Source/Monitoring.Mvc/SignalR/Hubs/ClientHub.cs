﻿using System;
using Microsoft.AspNetCore.SignalR;

namespace Monitoring.Mvc.SignalR.Hubs
{
    /// <summary>
    /// Концентратор, который служит в качестве конвейера высокого уровня для обработки взаимодействия
    /// между клиентом и сервером.
    /// </summary>
    public class ClientHub : Hub
    {
    }
}
