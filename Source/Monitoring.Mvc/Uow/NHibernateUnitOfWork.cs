﻿using System;
using System.Reflection;
using Monitoring.Mvc.Repositories;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;

namespace Monitoring.Mvc.Uow
{
    /// <summary>
    /// Логическая транзакция.
    /// </summary>
    public class NHibernateUnitOfWork : IUnitOfWork
    {
        private readonly ISession session;
        private readonly ITransaction transaction;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="connectionString">Строка подключения к БД.</param>
        public NHibernateUnitOfWork(string connectionString)
        {
            session = OpenSession(connectionString);
            transaction = session.BeginTransaction();
        }

        /// <summary>
        /// Сохранение изменений в транзакции.
        /// </summary>
        public void Commit()
        {
            if (transaction.IsActive)
            {
                transaction.Commit();
            }
        }

        /// <summary>
        /// Отмена изменений в транзакции и освобождение ресурсов.
        /// </summary>
        public void Dispose()
        {
            Rollback();
            transaction?.Dispose();
            session?.Dispose();
        }

        /// <summary>
        /// Получение репозитория событий устройств.
        /// </summary>
        /// <returns>Репозиторий событий устройств.</returns>
        public IDeviceEventRepository GetDeviceEventRepository()
        {
            return new NHibernateDeviceEventRepository(session);
        }

        /// <summary>
        /// Получение репозитория данных об устройствах.
        /// </summary>
        /// <returns>Репозиторий данных об устройствах.</returns>
        public IDeviceRepository GetDeviceRepository()
        {
            return new NHibernateDeviceRepository(session);
        }

        /// <summary>
        /// Отмена изменений в транзакции.
        /// </summary>
        public void Rollback()
        {
            if (transaction.IsActive)
            {
                transaction.Rollback();
            }
        }

        private ISession OpenSession(string connectionString)
        {
            var configuration = new Configuration()
                .DataBaseIntegration(db =>
                {
                    db.ConnectionString = connectionString;
                    db.Dialect<PostgreSQL83Dialect>();
                    db.Driver<NpgsqlDriver>();
                });

            var mapper = new ModelMapper();
            mapper.AddMappings(Assembly.GetExecutingAssembly().GetExportedTypes());

            HbmMapping mapping = mapper.CompileMappingForAllExplicitlyAddedEntities();
            configuration.AddMapping(mapping);

            new SchemaUpdate(configuration).Execute(true, true);

            ISessionFactory sessionFactory = configuration.BuildSessionFactory();

            return sessionFactory.OpenSession();
        }
    }
}
