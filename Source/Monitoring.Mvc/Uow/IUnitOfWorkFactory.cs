﻿using System;

namespace Monitoring.Mvc.Uow
{
    /// <summary>
    /// Интерфейс фабрики логических транзакций.
    /// </summary>
    public interface IUnitOfWorkFactory
    {
        /// <summary>
        /// Создание логической транзакции.
        /// </summary>
        /// <returns>Логическая транзакция.</returns>
        IUnitOfWork Create();
    }
}
