﻿using System;
using Monitoring.Mvc.Repositories;

namespace Monitoring.Mvc.Uow
{
    /// <summary>
    /// Интерфейс логической транзакции.
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Сохранение изменений в транзакции.
        /// </summary>
        void Commit();

        /// <summary>
        /// Получение репозитория событий устройств.
        /// </summary>
        /// <returns>Репозиторий событий устройств.</returns>
        IDeviceEventRepository GetDeviceEventRepository();

        /// <summary>
        /// Получение репозитория данных об устройствах.
        /// </summary>
        /// <returns>Репозиторий данных об устройствах.</returns>
        IDeviceRepository GetDeviceRepository();

        /// <summary>
        /// Отмена изменений в транзакции.
        /// </summary>
        void Rollback();
    }
}
