﻿using System;
using System.Data;
using Monitoring.Mvc.Repositories;
using Npgsql;

namespace Monitoring.Mvc.Uow
{
    /// <summary>
    /// Логическая транзакция.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private readonly NpgsqlConnection connection;
        private readonly NpgsqlTransaction transaction;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="connectionString">Строка подключения к БД.</param>
        public UnitOfWork(string connectionString)
        {
            connection = CreateConnection(connectionString);
            transaction = connection.BeginTransaction();
        }

        /// <summary>
        /// Сохранение изменений в транзакции.
        /// </summary>
        public void Commit()
        {
            if (!transaction.IsCompleted)
            {
                transaction.Commit();
            }
        }

        /// <summary>
        /// Отмена изменений в транзакции и освобождение ресурсов.
        /// </summary>
        public void Dispose()
        {
            Rollback();
            transaction?.DisposeAsync();
            connection?.DisposeAsync();
        }

        /// <summary>
        /// Получение репозитория событий устройств.
        /// </summary>
        /// <returns>Репозиторий событий устройств.</returns>
        public IDeviceEventRepository GetDeviceEventRepository()
        {
            return new DeviceEventRepository(connection as IDbConnection, transaction);
        }

        /// <summary>
        /// Получение репозитория данных об устройствах.
        /// </summary>
        /// <returns>Репозиторий данных об устройствах.</returns>
        public IDeviceRepository GetDeviceRepository()
        {
            return new DeviceRepository(connection as IDbConnection, transaction);
        }

        /// <summary>
        /// Отмена изменений в транзакции.
        /// </summary>
        public void Rollback()
        {
            if (!transaction.IsCompleted)
            {
                transaction.Rollback();
            }
        }

        private NpgsqlConnection CreateConnection(string connectionString)
        {
            var connection = new NpgsqlConnection(connectionString);
            connection.Open();

            return connection;
        }
    }
}
