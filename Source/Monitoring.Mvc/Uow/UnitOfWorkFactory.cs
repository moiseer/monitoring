﻿using System;
using Microsoft.Extensions.Configuration;

namespace Monitoring.Mvc.Uow
{
    /// <summary>
    /// Фабрика логических транзакций.
    /// </summary>
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly string connectionString;
        private readonly bool useNHibernate;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="configuration">Конфигурация приложения.</param>
        public UnitOfWorkFactory(IConfiguration configuration)
        {
            connectionString = configuration["ConnectionString"];
            useNHibernate = configuration.GetValue<bool>("UseNHibernate");
        }

        /// <summary>
        /// Создание логической транзакции.
        /// </summary>
        /// <returns>Логическая транзакция.</returns>
        public IUnitOfWork Create()
        {
            if (useNHibernate)
            {
                return new NHibernateUnitOfWork(connectionString);
            }

            return new UnitOfWork(connectionString);
        }
    }
}
