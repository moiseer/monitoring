#!/bin/bash
> ./postgres.env
> ./rabbitmq.env
> ./monitoring-mvc.env

if [ -n "$1" ]
then
pg_user=$1
else
read -p "Enter PostgreSQL username: " pg_user
fi
echo POSTGRES_USER=$pg_user >> ./postgres.env

if [ -n "$2" ]
then
pg_password=$2
else
read -s -p "Enter PostgreSQL password: " pg_password
fi
echo 
echo POSTGRES_PASSWORD=$pg_password >> ./postgres.env

echo "ConnectionString=Host=postgres;Port=5432;User Id=$pg_user;Password=$pg_password;Database=MonitoringDB;Pooling=true;Timeout=20;" >> ./monitoring-mvc.env

if [ -n "$3" ]
then
rabbit_user=$3
else
read -p "Enter RabbitMq username: " rabbit_user
fi
echo RABBITMQ_DEFAULT_USER=$rabbit_user >> ./rabbitmq.env

if [ -n "$4" ]
then
rabbit_password=$4
else
read -s -p "Enter RabbitMq password: " rabbit_password
fi
echo RABBITMQ_DEFAULT_PASS=$rabbit_password >> ./rabbitmq.env

echo "RabbitConnectionString=amqp://$rabbit_user:$rabbit_password@rabbitmq:5672" >> ./monitoring-mvc.env